<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('WP_CACHE', true); //Added by WP-Cache Manager
define( 'WPCACHEHOME', '/home1/nghia/public_html/domain/mobile/wp-content/plugins/wp-super-cache/' ); //Added by WP-Cache Manager
define('DB_NAME', 'mobile_antking');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '<82c|RE7qKD!g#I(BB!?ynF2\`>\`4^niJRgHK-tW-kkj/\`aVSDXC!jr^-(!?Af3z0-$O_<AP^2<6>gypZP_ZpP');
define('SECURE_AUTH_KEY',  '');
define('LOGGED_IN_KEY',    'wf*XBIkD4i)ZE-W~gHrePVQVv0\`8Nw6K=65rCOcF72<EDrfEfP1~T=~8(gmJt');
define('NONCE_KEY',        'mnbfvk1N:YWb~gD9O|laI^KcC5Ix6v2NnW\`X(QI1e0Fu-WrngTrBSsRN(W^T->CFK3TK_Tyl');
define('AUTH_SALT',        'wjJKM?cCI#|1r#@VUMg>|/ZaIwJ-DVspP_-5>AiF_>XG*JEea;UGghC@FG:KXmf=?H\`OU-Bxo|');
define('SECURE_AUTH_SALT', 'n/_NkJeihfjmi(?9\`8wf-c3)mzKC(zYq*=D<a/EsH5(7F<UwLvPq!;1HfG:amNwC3jOm\`5nPIEB');
define('LOGGED_IN_SALT',   '#dH<bS_:WsvA~KE=voE/cEXgDUX7fO4d0^2BL$nA:4z44>$hX0!DdHrYhvhVh5CHPKJUv@JJ');
define('NONCE_SALT',       'f4Hhm|D(YY5yoREluO\`NIFd_Cx7KT$9)zZVrXsN0Akx=X2AGVw0|pQM5$>2n2\`8wl?Svq5:aCQ_M');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
