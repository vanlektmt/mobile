-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 23, 2013 at 10:36 AM
-- Server version: 5.6.14
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mobile_antking`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_commentmeta`
--

CREATE TABLE IF NOT EXISTS `wp_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `wp_comments`
--

CREATE TABLE IF NOT EXISTS `wp_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext NOT NULL,
  `comment_author_email` varchar(100) NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) NOT NULL DEFAULT '',
  `comment_type` varchar(20) NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Mr WordPress', '', 'http://wordpress.org/', '', '2013-10-29 15:43:53', '2013-10-29 15:43:53', 'Hi, this is a comment.\nTo delete a comment, just log in and view the post&#039;s comments. There you will have the option to edit or delete them.', 0, 'post-trashed', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_links`
--

CREATE TABLE IF NOT EXISTS `wp_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) NOT NULL DEFAULT '',
  `link_name` varchar(255) NOT NULL DEFAULT '',
  `link_image` varchar(255) NOT NULL DEFAULT '',
  `link_target` varchar(25) NOT NULL DEFAULT '',
  `link_description` varchar(255) NOT NULL DEFAULT '',
  `link_visible` varchar(20) NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) NOT NULL DEFAULT '',
  `link_notes` mediumtext NOT NULL,
  `link_rss` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `wp_options`
--

CREATE TABLE IF NOT EXISTS `wp_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(64) NOT NULL DEFAULT '',
  `option_value` longtext NOT NULL,
  `autoload` varchar(20) NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=461 ;

--
-- Dumping data for table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://mobile.antking.net', 'yes'),
(2, 'blogname', 'Antking', 'yes'),
(3, 'blogdescription', 'Technical Blog', 'yes'),
(4, 'users_can_register', '0', 'yes'),
(5, 'admin_email', 'hoangdinh@antking.com.vn', 'yes'),
(6, 'start_of_week', '1', 'yes'),
(7, 'use_balanceTags', '0', 'yes'),
(8, 'use_smilies', '1', 'yes'),
(9, 'require_name_email', '1', 'yes'),
(10, 'comments_notify', '1', 'yes'),
(11, 'posts_per_rss', '10', 'yes'),
(12, 'rss_use_excerpt', '0', 'yes'),
(13, 'mailserver_url', 'mail.example.com', 'yes'),
(14, 'mailserver_login', 'login@example.com', 'yes'),
(15, 'mailserver_pass', 'password', 'yes'),
(16, 'mailserver_port', '110', 'yes'),
(17, 'default_category', '1', 'yes'),
(18, 'default_comment_status', 'open', 'yes'),
(19, 'default_ping_status', 'open', 'yes'),
(20, 'default_pingback_flag', '1', 'yes'),
(21, 'posts_per_page', '10', 'yes'),
(22, 'date_format', 'F j, Y', 'yes'),
(23, 'time_format', 'g:i a', 'yes'),
(24, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(25, 'links_recently_updated_prepend', '<em>', 'yes'),
(26, 'links_recently_updated_append', '</em>', 'yes'),
(27, 'links_recently_updated_time', '120', 'yes'),
(28, 'comment_moderation', '0', 'yes'),
(29, 'moderation_notify', '1', 'yes'),
(30, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(31, 'gzipcompression', '0', 'yes'),
(32, 'hack_file', '0', 'yes'),
(33, 'blog_charset', 'UTF-8', 'yes'),
(34, 'moderation_keys', '', 'no'),
(35, 'active_plugins', 'a:2:{i:0;s:39:"options-framework/options-framework.php";i:1;s:37:"post-types-order/post-types-order.php";}', 'yes'),
(36, 'home', 'http://mobile.antking.net', 'yes'),
(37, 'category_base', '', 'yes'),
(38, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(39, 'advanced_edit', '0', 'yes'),
(40, 'comment_max_links', '2', 'yes'),
(41, 'gmt_offset', '0', 'yes'),
(42, 'default_email_category', '1', 'yes'),
(43, 'recently_edited', '', 'no'),
(44, 'template', 'Quora', 'yes'),
(45, 'stylesheet', 'Quora', 'yes'),
(46, 'comment_whitelist', '1', 'yes'),
(47, 'blacklist_keys', '', 'no'),
(48, 'comment_registration', '0', 'yes'),
(49, 'html_type', 'text/html', 'yes'),
(50, 'use_trackback', '0', 'yes'),
(51, 'default_role', 'subscriber', 'yes'),
(52, 'db_version', '25824', 'yes'),
(53, 'uploads_use_yearmonth_folders', '1', 'yes'),
(54, 'upload_path', '', 'yes'),
(55, 'blog_public', '1', 'yes'),
(56, 'default_link_category', '2', 'yes'),
(57, 'show_on_front', 'posts', 'yes'),
(58, 'tag_base', '', 'yes'),
(59, 'show_avatars', '1', 'yes'),
(60, 'avatar_rating', 'G', 'yes'),
(61, 'upload_url_path', '', 'yes'),
(62, 'thumbnail_size_w', '150', 'yes'),
(63, 'thumbnail_size_h', '150', 'yes'),
(64, 'thumbnail_crop', '1', 'yes'),
(65, 'medium_size_w', '300', 'yes'),
(66, 'medium_size_h', '300', 'yes'),
(67, 'avatar_default', 'mystery', 'yes'),
(68, 'large_size_w', '1024', 'yes'),
(69, 'large_size_h', '1024', 'yes'),
(70, 'image_default_link_type', 'file', 'yes'),
(71, 'image_default_size', '', 'yes'),
(72, 'image_default_align', '', 'yes'),
(73, 'close_comments_for_old_posts', '0', 'yes'),
(74, 'close_comments_days_old', '14', 'yes'),
(75, 'thread_comments', '1', 'yes'),
(76, 'thread_comments_depth', '5', 'yes'),
(77, 'page_comments', '0', 'yes'),
(78, 'comments_per_page', '50', 'yes'),
(79, 'default_comments_page', 'newest', 'yes'),
(80, 'comment_order', 'asc', 'yes'),
(81, 'sticky_posts', 'a:0:{}', 'yes'),
(82, 'widget_categories', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(83, 'widget_text', 'a:0:{}', 'yes'),
(84, 'widget_rss', 'a:0:{}', 'yes'),
(85, 'uninstall_plugins', 'a:1:{s:27:"wp-super-cache/wp-cache.php";s:23:"wpsupercache_deactivate";}', 'no'),
(86, 'timezone_string', '', 'yes'),
(87, 'page_for_posts', '0', 'yes'),
(88, 'page_on_front', '0', 'yes'),
(89, 'default_post_format', '0', 'yes'),
(90, 'link_manager_enabled', '0', 'yes'),
(91, 'initial_db_version', '25824', 'yes'),
(92, 'wp_user_roles', 'a:5:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:62:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:9:"add_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:34:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}}', 'yes'),
(93, 'widget_search', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(94, 'widget_recent-posts', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(95, 'widget_recent-comments', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(96, 'widget_archives', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(97, 'widget_meta', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(98, 'sidebars_widgets', 'a:3:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:0:{}s:13:"array_version";i:3;}', 'yes'),
(99, 'cron', 'a:6:{i:1387792798;a:1:{s:19:"wp_cache_gc_watcher";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:6:"hourly";s:4:"args";a:0:{}s:8:"interval";i:3600;}}}i:1387813435;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1387813441;a:1:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1387825200;a:1:{s:20:"wp_maybe_auto_update";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1387875489;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}s:7:"version";i:2;}', 'yes'),
(168, '_site_transient_browser_d11ee7bd62d3beee9230c318e77cae0a', 'a:9:{s:8:"platform";s:7:"Windows";s:4:"name";s:6:"Chrome";s:7:"version";s:12:"31.0.1650.57";s:10:"update_url";s:28:"http://www.google.com/chrome";s:7:"img_src";s:49:"http://s.wordpress.org/images/browsers/chrome.png";s:11:"img_src_ssl";s:48:"https://wordpress.org/images/browsers/chrome.png";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'yes'),
(166, '_site_transient_update_core', 'O:8:"stdClass":4:{s:7:"updates";a:2:{i:0;O:8:"stdClass":10:{s:8:"response";s:7:"upgrade";s:8:"download";s:39:"https://wordpress.org/wordpress-3.8.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:39:"https://wordpress.org/wordpress-3.8.zip";s:10:"no_content";s:50:"https://wordpress.org/wordpress-3.8-no-content.zip";s:11:"new_bundled";s:51:"https://wordpress.org/wordpress-3.8-new-bundled.zip";s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:3:"3.8";s:7:"version";s:3:"3.8";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"3.8";s:15:"partial_version";s:0:"";}i:1;O:8:"stdClass":10:{s:8:"response";s:10:"autoupdate";s:8:"download";s:39:"https://wordpress.org/wordpress-3.8.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:39:"https://wordpress.org/wordpress-3.8.zip";s:10:"no_content";s:50:"https://wordpress.org/wordpress-3.8-no-content.zip";s:11:"new_bundled";s:51:"https://wordpress.org/wordpress-3.8-new-bundled.zip";s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:3:"3.8";s:7:"version";s:3:"3.8";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"3.8";s:15:"partial_version";s:0:"";}}s:12:"last_checked";i:1387772716;s:15:"version_checked";s:5:"3.7.1";s:12:"translations";a:0:{}}', 'yes'),
(203, '_site_transient_update_themes', 'O:8:"stdClass":4:{s:12:"last_checked";i:1387772828;s:7:"checked";a:4:{s:5:"Quora";s:5:"1.0.0";s:9:"imbalance";s:4:"1.19";s:14:"twentythirteen";s:3:"1.1";s:12:"twentytwelve";s:3:"1.3";}s:8:"response";a:0:{}s:12:"translations";a:0:{}}', 'yes'),
(428, '_site_transient_timeout_theme_roots', '1387774614', 'yes'),
(429, '_site_transient_theme_roots', 'a:4:{s:5:"Quora";s:7:"/themes";s:9:"imbalance";s:7:"/themes";s:14:"twentythirteen";s:7:"/themes";s:12:"twentytwelve";s:7:"/themes";}', 'yes'),
(106, '_site_transient_timeout_browser_7b3c8f5515f4dae0b11f5c777183690e', '1383666241', 'yes'),
(107, '_site_transient_browser_7b3c8f5515f4dae0b11f5c777183690e', 'a:9:{s:8:"platform";s:5:"Linux";s:4:"name";s:6:"Chrome";s:7:"version";s:12:"29.0.1547.65";s:10:"update_url";s:28:"http://www.google.com/chrome";s:7:"img_src";s:49:"http://s.wordpress.org/images/browsers/chrome.png";s:11:"img_src_ssl";s:48:"https://wordpress.org/images/browsers/chrome.png";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'yes'),
(108, 'dashboard_widget_options', 'a:4:{s:25:"dashboard_recent_comments";a:1:{s:5:"items";i:5;}s:24:"dashboard_incoming_links";a:5:{s:4:"home";s:34:"http://localhost.mobileantking.com";s:4:"link";s:110:"http://blogsearch.google.com/blogsearch?scoring=d&partner=wordpress&q=link:http://localhost.mobileantking.com/";s:3:"url";s:134:"http://blogsearch.google.com/blogsearch_feeds?scoring=d&ie=utf-8&num=10&output=rss&partner=wordpress&q=link:http://mobile.antking.net/";s:5:"items";i:10;s:9:"show_date";b:0;}s:17:"dashboard_primary";a:7:{s:4:"link";s:26:"http://wordpress.org/news/";s:3:"url";s:31:"http://wordpress.org/news/feed/";s:5:"title";s:14:"WordPress Blog";s:5:"items";i:2;s:12:"show_summary";i:1;s:11:"show_author";i:0;s:9:"show_date";i:1;}s:19:"dashboard_secondary";a:7:{s:4:"link";s:28:"http://planet.wordpress.org/";s:3:"url";s:33:"http://planet.wordpress.org/feed/";s:5:"title";s:20:"Other WordPress News";s:5:"items";i:5;s:12:"show_summary";i:0;s:11:"show_author";i:0;s:9:"show_date";i:0;}}', 'yes'),
(117, '_transient_timeout_feed_6ec23663af434a80564d615b2cb00a03', '1383104644', 'no'),
(118, '_transient_feed_6ec23663af434a80564d615b2cb00a03', 'a:4:{s:5:"child";a:1:{s:0:"";a:1:{s:3:"rss";a:1:{i:0;a:6:{s:4:"data";s:4:"\n  \n";s:7:"attribs";a:1:{s:0:"";a:1:{s:7:"version";s:3:"2.0";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:1:{s:7:"channel";a:1:{i:0;a:6:{s:4:"data";s:33:"\n    \n    \n    \n    \n    \n    \n  ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:3:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:63:"link:http://mobile.antking.net// - Google Blog Search";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:100:"http://www.google.com/search?ie=utf-8&q=link:http://mobile.antking.net//&tbm=blg&tbs=sbd:1";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:94:"Your search - <b>link:http://mobile.antking.net//</b> - did not match any documents.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://a9.com/-/spec/opensearch/1.1/";a:3:{s:12:"totalResults";a:1:{i:0;a:5:{s:4:"data";s:1:"0";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:10:"startIndex";a:1:{i:0;a:5:{s:4:"data";s:1:"1";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:12:"itemsPerPage";a:1:{i:0;a:5:{s:4:"data";s:2:"10";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}}}}}}s:4:"type";i:128;s:7:"headers";a:10:{s:12:"content-type";s:28:"text/xml; charset=ISO-8859-1";s:4:"date";s:29:"Tue, 29 Oct 2013 15:44:05 GMT";s:7:"expires";s:2:"-1";s:13:"cache-control";s:18:"private, max-age=0";s:10:"set-cookie";a:2:{i:0;s:143:"PREF=ID=e04224a6d3b1c5e3:FF=0:TM=1383061445:LM=1383061445:S=re77BcHadmHLqBc0; expires=Thu, 29-Oct-2015 15:44:05 GMT; path=/; domain=.google.com";i:1;s:212:"NID=67=CIH5oFUeEDTomXZT3r5pCLlowgypL7ySZ9HJ0w_RNro-GwKDVdo1wJIz13YclPQNBZQwAuh4R2TD6OUr77p6PXLcjp4O6hY1lrAki-22gLXVirLBR8-nyJR9t25g66As; expires=Wed, 30-Apr-2014 15:44:05 GMT; path=/; domain=.google.com; HttpOnly";}s:3:"p3p";s:122:"CP="This is not a P3P policy! See http://www.google.com/support/accounts/bin/answer.py?hl=en&answer=151657 for more info."";s:6:"server";s:3:"gws";s:16:"x-xss-protection";s:13:"1; mode=block";s:15:"x-frame-options";s:10:"SAMEORIGIN";s:18:"alternate-protocol";s:7:"80:quic";}s:5:"build";s:14:"20130911040210";}', 'no'),
(119, '_transient_timeout_feed_mod_6ec23663af434a80564d615b2cb00a03', '1383104644', 'no'),
(120, '_transient_feed_mod_6ec23663af434a80564d615b2cb00a03', '1383061444', 'no'),
(432, '_transient_timeout_feed_mod_a02ec38ecc2c7a647c50178532525608', '1387816037', 'no'),
(433, '_transient_feed_mod_a02ec38ecc2c7a647c50178532525608', '1387772837', 'no'),
(434, '_transient_timeout_dash_20494a3d90a6669585674ed0eb8dcd8f', '1387816037', 'no'),
(435, '_transient_dash_20494a3d90a6669585674ed0eb8dcd8f', '<p>This dashboard widget queries <a href="http://blogsearch.google.com/">Google Blog Search</a> so that when another blog links to your site it will show up here. It has found no incoming links&hellip; yet. It&#8217;s okay &#8212; there is no rush.</p>\n', 'no'),
(436, '_transient_timeout_dash_aa95765b5cc111c56d5993d476b1c2f0', '1387816047', 'no'),
(437, '_transient_dash_aa95765b5cc111c56d5993d476b1c2f0', '<div class="rss-widget"><p><strong>RSS Error</strong>: WP HTTP Error: Operation timed out after 10015 milliseconds with 0 out of -1 bytes received</p></div>', 'no'),
(438, '_transient_timeout_dash_4077549d03da2e451c8b5f002294ff51', '1387816047', 'no'),
(439, '_transient_dash_4077549d03da2e451c8b5f002294ff51', '<div class="rss-widget"><p><strong>RSS Error</strong>: WP HTTP Error: Operation timed out after 10015 milliseconds with 0 out of -1 bytes received</p></div>', 'no'),
(440, 'auto_updater.lock', '1387782078', 'no'),
(430, '_transient_timeout_feed_a02ec38ecc2c7a647c50178532525608', '1387816037', 'no'),
(431, '_transient_feed_a02ec38ecc2c7a647c50178532525608', 'a:4:{s:5:"child";a:1:{s:0:"";a:1:{s:3:"rss";a:1:{i:0;a:6:{s:4:"data";s:4:"\n  \n";s:7:"attribs";a:1:{s:0:"";a:1:{s:7:"version";s:3:"2.0";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:1:{s:7:"channel";a:1:{i:0;a:6:{s:4:"data";s:33:"\n    \n    \n    \n    \n    \n    \n  ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:3:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:52:"link:http://mobile.antking.net/ - Google Blog Search";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:89:"http://www.google.com/search?ie=utf-8&q=link:http://mobile.antking.net/&tbm=blg&tbs=sbd:1";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:83:"Your search - <b>link:http://mobile.antking.net/</b> - did not match any documents.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://a9.com/-/spec/opensearch/1.1/";a:3:{s:12:"totalResults";a:1:{i:0;a:5:{s:4:"data";s:1:"0";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:10:"startIndex";a:1:{i:0;a:5:{s:4:"data";s:1:"1";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:12:"itemsPerPage";a:1:{i:0;a:5:{s:4:"data";s:2:"10";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}}}}}}s:4:"type";i:128;s:7:"headers";a:10:{s:12:"content-type";s:28:"text/xml; charset=ISO-8859-1";s:4:"date";s:29:"Mon, 23 Dec 2013 04:27:38 GMT";s:7:"expires";s:2:"-1";s:13:"cache-control";s:18:"private, max-age=0";s:10:"set-cookie";a:2:{i:0;s:143:"PREF=ID=68bb848aff859af8:FF=0:TM=1387772858:LM=1387772858:S=SnRDxMzWSPAVlyxO; expires=Wed, 23-Dec-2015 04:27:38 GMT; path=/; domain=.google.com";i:1;s:212:"NID=67=tu4fHl62579al_eBodJO88Ht8Yv-rMjavjvJBvoD4tbxcOnBKhuE5lgBD8wIzPJmIsScOws_GIUUdgkSRFApfWVSYTV3IJcNE7bCAU7emp3DByCXGceWuQUQsLzRI7wo; expires=Tue, 24-Jun-2014 04:27:38 GMT; path=/; domain=.google.com; HttpOnly";}s:3:"p3p";s:122:"CP="This is not a P3P policy! See http://www.google.com/support/accounts/bin/answer.py?hl=en&answer=151657 for more info."";s:6:"server";s:3:"gws";s:16:"x-xss-protection";s:13:"1; mode=block";s:15:"x-frame-options";s:10:"SAMEORIGIN";s:18:"alternate-protocol";s:7:"80:quic";}s:5:"build";s:14:"20131216081310";}', 'no'),
(295, '_site_transient_timeout_browser_62a0a1701fd4212f739e7ec94036c112', '1387253194', 'yes'),
(296, '_site_transient_browser_62a0a1701fd4212f739e7ec94036c112', 'a:9:{s:8:"platform";s:7:"Windows";s:4:"name";s:6:"Chrome";s:7:"version";s:12:"28.0.1500.72";s:10:"update_url";s:28:"http://www.google.com/chrome";s:7:"img_src";s:49:"http://s.wordpress.org/images/browsers/chrome.png";s:11:"img_src_ssl";s:48:"https://wordpress.org/images/browsers/chrome.png";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'yes'),
(204, 'theme_mods_twentythirteen', 'a:1:{s:16:"sidebars_widgets";a:2:{s:4:"time";i:1384935420;s:4:"data";a:3:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:9:"sidebar-2";a:0:{}}}}', 'yes'),
(205, 'current_theme', 'Quora', 'yes'),
(206, 'theme_mods_imbalance', 'a:8:{i:0;b:0;s:18:"nav_menu_locations";a:1:{s:11:"custom-menu";i:2;}s:16:"background_color";s:0:"";s:16:"background_image";s:0:"";s:17:"background_repeat";s:6:"repeat";s:21:"background_position_x";s:4:"left";s:21:"background_attachment";s:5:"fixed";s:16:"sidebars_widgets";a:2:{s:4:"time";i:1387184010;s:4:"data";a:2:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}}}}', 'yes'),
(207, 'theme_switched', '', 'yes'),
(348, 'category_children', 'a:0:{}', 'yes'),
(210, 'imbalance_fbkurl', '', 'yes'),
(211, 'imbalance_twturl', '', 'yes'),
(212, 'imbalance_excln', '', 'yes'),
(213, 'imbalance_custom_logo', 'http://mobile.antking.net/wp-content/uploads/2013/11/logo.png', 'yes'),
(214, 'imbalance_gallery_off', '', 'yes'),
(215, 'imbalance_sidebar_off', '', 'yes'),
(216, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:"auto_add";a:0:{}}', 'yes'),
(262, '_site_transient_timeout_browser_df9bb5d7548b92730d5c33af0b99d718', '1387252442', 'yes'),
(263, '_site_transient_browser_df9bb5d7548b92730d5c33af0b99d718', 'a:9:{s:8:"platform";s:9:"Macintosh";s:4:"name";s:6:"Chrome";s:7:"version";s:12:"31.0.1650.48";s:10:"update_url";s:28:"http://www.google.com/chrome";s:7:"img_src";s:49:"http://s.wordpress.org/images/browsers/chrome.png";s:11:"img_src_ssl";s:48:"https://wordpress.org/images/browsers/chrome.png";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'yes'),
(398, '_site_transient_wporg_theme_feature_list', 'a:5:{s:6:"Colors";a:15:{i:0;s:5:"black";i:1;s:4:"blue";i:2;s:5:"brown";i:3;s:4:"gray";i:4;s:5:"green";i:5;s:6:"orange";i:6;s:4:"pink";i:7;s:6:"purple";i:8;s:3:"red";i:9;s:6:"silver";i:10;s:3:"tan";i:11;s:5:"white";i:12;s:6:"yellow";i:13;s:4:"dark";i:14;s:5:"light";}s:7:"Columns";a:6:{i:0;s:10:"one-column";i:1;s:11:"two-columns";i:2;s:13:"three-columns";i:3;s:12:"four-columns";i:4;s:12:"left-sidebar";i:5;s:13:"right-sidebar";}s:8:"Features";a:19:{i:1;s:8:"blavatar";i:2;s:10:"buddypress";i:3;s:17:"custom-background";i:4;s:13:"custom-colors";i:5;s:13:"custom-header";i:6;s:11:"custom-menu";i:7;s:12:"editor-style";i:8;s:21:"featured-image-header";i:9;s:15:"featured-images";i:10;s:15:"flexible-header";i:11;s:20:"front-page-post-form";i:12;s:19:"full-width-template";i:13;s:12:"microformats";i:14;s:12:"post-formats";i:15;s:20:"rtl-language-support";i:16;s:11:"sticky-post";i:17;s:13:"theme-options";i:18;s:17:"threaded-comments";i:19;s:17:"translation-ready";}s:7:"Subject";a:3:{i:0;s:7:"holiday";i:1;s:13:"photoblogging";i:2;s:8:"seasonal";}s:5:"Width";a:2:{i:0;s:11:"fixed-width";i:1;s:14:"flexible-width";}}', 'yes'),
(135, 'recently_activated', 'a:0:{}', 'yes'),
(142, 'can_compress_scripts', '1', 'yes'),
(199, '_site_transient_timeout_poptags_40cd750bba9870f18aada2478b24840a', '1384946180', 'yes'),
(200, '_site_transient_poptags_40cd750bba9870f18aada2478b24840a', 'a:40:{s:6:"widget";a:3:{s:4:"name";s:6:"widget";s:4:"slug";s:6:"widget";s:5:"count";s:4:"3898";}s:4:"post";a:3:{s:4:"name";s:4:"Post";s:4:"slug";s:4:"post";s:5:"count";s:4:"2456";}s:6:"plugin";a:3:{s:4:"name";s:6:"plugin";s:4:"slug";s:6:"plugin";s:5:"count";s:4:"2344";}s:5:"admin";a:3:{s:4:"name";s:5:"admin";s:4:"slug";s:5:"admin";s:5:"count";s:4:"1930";}s:5:"posts";a:3:{s:4:"name";s:5:"posts";s:4:"slug";s:5:"posts";s:5:"count";s:4:"1856";}s:7:"sidebar";a:3:{s:4:"name";s:7:"sidebar";s:4:"slug";s:7:"sidebar";s:5:"count";s:4:"1583";}s:7:"twitter";a:3:{s:4:"name";s:7:"twitter";s:4:"slug";s:7:"twitter";s:5:"count";s:4:"1329";}s:6:"google";a:3:{s:4:"name";s:6:"google";s:4:"slug";s:6:"google";s:5:"count";s:4:"1325";}s:8:"comments";a:3:{s:4:"name";s:8:"comments";s:4:"slug";s:8:"comments";s:5:"count";s:4:"1310";}s:6:"images";a:3:{s:4:"name";s:6:"images";s:4:"slug";s:6:"images";s:5:"count";s:4:"1260";}s:4:"page";a:3:{s:4:"name";s:4:"page";s:4:"slug";s:4:"page";s:5:"count";s:4:"1225";}s:5:"image";a:3:{s:4:"name";s:5:"image";s:4:"slug";s:5:"image";s:5:"count";s:4:"1121";}s:9:"shortcode";a:3:{s:4:"name";s:9:"shortcode";s:4:"slug";s:9:"shortcode";s:5:"count";s:4:"1000";}s:8:"facebook";a:3:{s:4:"name";s:8:"Facebook";s:4:"slug";s:8:"facebook";s:5:"count";s:3:"982";}s:5:"links";a:3:{s:4:"name";s:5:"links";s:4:"slug";s:5:"links";s:5:"count";s:3:"974";}s:3:"seo";a:3:{s:4:"name";s:3:"seo";s:4:"slug";s:3:"seo";s:5:"count";s:3:"950";}s:9:"wordpress";a:3:{s:4:"name";s:9:"wordpress";s:4:"slug";s:9:"wordpress";s:5:"count";s:3:"844";}s:7:"gallery";a:3:{s:4:"name";s:7:"gallery";s:4:"slug";s:7:"gallery";s:5:"count";s:3:"821";}s:6:"social";a:3:{s:4:"name";s:6:"social";s:4:"slug";s:6:"social";s:5:"count";s:3:"780";}s:3:"rss";a:3:{s:4:"name";s:3:"rss";s:4:"slug";s:3:"rss";s:5:"count";s:3:"722";}s:7:"widgets";a:3:{s:4:"name";s:7:"widgets";s:4:"slug";s:7:"widgets";s:5:"count";s:3:"686";}s:6:"jquery";a:3:{s:4:"name";s:6:"jquery";s:4:"slug";s:6:"jquery";s:5:"count";s:3:"681";}s:5:"pages";a:3:{s:4:"name";s:5:"pages";s:4:"slug";s:5:"pages";s:5:"count";s:3:"678";}s:5:"email";a:3:{s:4:"name";s:5:"email";s:4:"slug";s:5:"email";s:5:"count";s:3:"623";}s:4:"ajax";a:3:{s:4:"name";s:4:"AJAX";s:4:"slug";s:4:"ajax";s:5:"count";s:3:"615";}s:5:"media";a:3:{s:4:"name";s:5:"media";s:4:"slug";s:5:"media";s:5:"count";s:3:"595";}s:10:"javascript";a:3:{s:4:"name";s:10:"javascript";s:4:"slug";s:10:"javascript";s:5:"count";s:3:"572";}s:5:"video";a:3:{s:4:"name";s:5:"video";s:4:"slug";s:5:"video";s:5:"count";s:3:"570";}s:10:"buddypress";a:3:{s:4:"name";s:10:"buddypress";s:4:"slug";s:10:"buddypress";s:5:"count";s:3:"541";}s:4:"feed";a:3:{s:4:"name";s:4:"feed";s:4:"slug";s:4:"feed";s:5:"count";s:3:"539";}s:7:"content";a:3:{s:4:"name";s:7:"content";s:4:"slug";s:7:"content";s:5:"count";s:3:"530";}s:5:"photo";a:3:{s:4:"name";s:5:"photo";s:4:"slug";s:5:"photo";s:5:"count";s:3:"522";}s:4:"link";a:3:{s:4:"name";s:4:"link";s:4:"slug";s:4:"link";s:5:"count";s:3:"506";}s:6:"photos";a:3:{s:4:"name";s:6:"photos";s:4:"slug";s:6:"photos";s:5:"count";s:3:"505";}s:5:"login";a:3:{s:4:"name";s:5:"login";s:4:"slug";s:5:"login";s:5:"count";s:3:"471";}s:4:"spam";a:3:{s:4:"name";s:4:"spam";s:4:"slug";s:4:"spam";s:5:"count";s:3:"458";}s:5:"stats";a:3:{s:4:"name";s:5:"stats";s:4:"slug";s:5:"stats";s:5:"count";s:3:"453";}s:8:"category";a:3:{s:4:"name";s:8:"category";s:4:"slug";s:8:"category";s:5:"count";s:3:"452";}s:7:"youtube";a:3:{s:4:"name";s:7:"youtube";s:4:"slug";s:7:"youtube";s:5:"count";s:3:"436";}s:7:"comment";a:3:{s:4:"name";s:7:"comment";s:4:"slug";s:7:"comment";s:5:"count";s:3:"432";}}', 'yes'),
(397, '_site_transient_timeout_wporg_theme_feature_list', '1387231692', 'yes'),
(149, 'rewrite_rules', 'a:70:{s:47:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:42:"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:44:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:39:"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:14:"tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:45:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:40:"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:32:"feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:27:"(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:41:"comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:36:"comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:44:"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:39:"search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:47:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:42:"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:58:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:68:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:88:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:83:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:83:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$";s:85:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1";s:77:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]";s:72:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]";s:65:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$";s:98:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]";s:72:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$";s:98:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(/[0-9]+)?/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]";s:47:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:57:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:77:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:72:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:72:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]";s:51:"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]";s:38:"([0-9]{4})/comment-page-([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&cpage=$matches[2]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:20:"(.?.+?)(/[0-9]+)?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";}', 'yes'),
(150, 'ossdl_off_cdn_url', 'http://mobile.antking.net/', 'yes'),
(151, 'ossdl_off_include_dirs', 'wp-content,wp-includes', 'yes'),
(152, 'ossdl_off_exclude', '.php', 'yes'),
(153, 'ossdl_cname', '', 'yes'),
(154, 'wpsupercache_start', '1383061679', 'yes'),
(155, 'wpsupercache_count', '0', 'yes'),
(157, 'supercache_stats', 'a:3:{s:9:"generated";i:1383061956;s:10:"supercache";a:5:{s:7:"expired";i:0;s:12:"expired_list";a:0:{}s:6:"cached";i:0;s:11:"cached_list";a:0:{}s:2:"ts";i:1383061956;}s:7:"wpcache";a:3:{s:6:"cached";i:0;s:7:"expired";i:0;s:5:"fsize";s:3:"0KB";}}', 'yes'),
(167, '_site_transient_timeout_browser_d11ee7bd62d3beee9230c318e77cae0a', '1385540172', 'yes'),
(302, '_site_transient_timeout_browser_2a45585bf6fbca9e24972054edb9a714', '1387788793', 'yes'),
(426, '_site_transient_update_plugins', 'O:8:"stdClass":3:{s:12:"last_checked";i:1387782078;s:8:"response";a:1:{s:19:"jetpack/jetpack.php";O:8:"stdClass":5:{s:2:"id";s:5:"20101";s:4:"slug";s:7:"jetpack";s:11:"new_version";s:3:"2.7";s:3:"url";s:37:"http://wordpress.org/plugins/jetpack/";s:7:"package";s:53:"http://downloads.wordpress.org/plugin/jetpack.2.7.zip";}}s:12:"translations";a:0:{}}', 'yes'),
(303, '_site_transient_browser_2a45585bf6fbca9e24972054edb9a714', 'a:9:{s:8:"platform";s:7:"Windows";s:4:"name";s:6:"Chrome";s:7:"version";s:12:"31.0.1650.63";s:10:"update_url";s:28:"http://www.google.com/chrome";s:7:"img_src";s:49:"http://s.wordpress.org/images/browsers/chrome.png";s:11:"img_src_ssl";s:48:"https://wordpress.org/images/browsers/chrome.png";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'yes'),
(445, '_transient_timeout_plugin_slugs', '1387873580', 'no'),
(446, '_transient_plugin_slugs', 'a:5:{i:0;s:19:"akismet/akismet.php";i:1;s:9:"hello.php";i:2;s:19:"jetpack/jetpack.php";i:3;s:39:"options-framework/options-framework.php";i:4;s:37:"post-types-order/post-types-order.php";}', 'no'),
(447, '_transient_timeout_dash_de3249c4736ad3bd2cd29147c4a0d43e', '1387830380', 'no'),
(448, '_transient_dash_de3249c4736ad3bd2cd29147c4a0d43e', '', 'no'),
(336, 'theme_mods_Quora', 'a:2:{i:0;b:0;s:18:"nav_menu_locations";a:1:{s:7:"primary";i:0;}}', 'yes'),
(337, 'cpto_options', 'a:3:{s:8:"autosort";s:1:"1";s:9:"adminsort";s:1:"1";s:10:"capability";s:13:"publish_posts";}', 'yes'),
(338, 'optionsframework', 'a:1:{s:2:"id";s:5:"quora";}', 'yes'),
(339, 'CPT_configured', 'TRUE', 'yes'),
(340, 'ffref', '214518', 'yes'),
(341, 'fflink', 'Website by Wordpress', 'yes'),
(375, 'quora', 'a:19:{s:8:"w2f_logo";s:71:"http://localhost.mobileantking.com/wp-content/uploads/2013/12/logo1.png";s:20:"w2f_slide_categories";s:1:"3";s:16:"w2f_slide_number";s:0:"";s:11:"w2f_banner1";s:44:"http://web2feel.com/images/webhostinghub.png";s:8:"w2f_alt1";s:43:"Reliable web hosting from WebHostingHub.com";s:8:"w2f_url1";s:0:"";s:8:"w2f_lab1";s:45:"Web Hosting Hub - Cheap reliable web hosting.";s:11:"w2f_banner2";s:38:"http://web2feel.com/images/pcnames.png";s:8:"w2f_alt2";s:56:"Domain name search and availability check by PCNames.com";s:8:"w2f_url2";s:23:"http://www.pcnames.com/";s:8:"w2f_lab2";s:53:"PC Names - Domain name search and availability check.";s:11:"w2f_banner3";s:44:"http://web2feel.com/images/designcontest.png";s:8:"w2f_alt3";s:54:"Website and logo design contests at DesignContest.com.";s:8:"w2f_url3";s:29:"http://www.designcontest.com/";s:8:"w2f_lab3";s:50:"Design Contest - Logo and website design contests.";s:11:"w2f_banner4";s:47:"http://web2feel.com/images/webhostingrating.png";s:8:"w2f_alt4";s:71:"Reviews of the best cheap web hosting providers at WebHostingRating.com";s:8:"w2f_url4";s:27:"http://webhostingrating.com";s:8:"w2f_lab4";s:59:"Web Hosting Rating - Customer reviews of the best web hosts";}', 'yes'),
(381, 'external_theme_updates-Quora', 'O:8:"stdClass":3:{s:9:"lastCheck";i:1387772818;s:14:"checkedVersion";s:5:"1.0.0";s:6:"update";N;}', 'yes'),
(402, 'color_scheme', '#29B977', 'yes'),
(403, 'color_secondary', '#05a869', 'yes'),
(404, 'link_color', '#C22443', 'yes'),
(405, 'hover_link_color', '#EF2349', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `wp_postmeta`
--

CREATE TABLE IF NOT EXISTS `wp_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=208 ;

--
-- Dumping data for table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(5, 6, '_wp_attached_file', '2013/10/WittyDoc.png'),
(4, 1, '_edit_lock', '1387193902:1'),
(6, 6, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1200;s:6:"height";i:1200;s:4:"file";s:20:"2013/10/WittyDoc.png";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"WittyDoc-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:20:"WittyDoc-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:22:"WittyDoc-1024x1024.png";s:5:"width";i:1024;s:6:"height";i:1024;s:9:"mime-type";s:9:"image/png";}s:14:"post-thumbnail";a:4:{s:4:"file";s:20:"WittyDoc-305x305.png";s:5:"width";i:305;s:6:"height";i:305;s:9:"mime-type";s:9:"image/png";}s:10:"background";a:4:{s:4:"file";s:20:"WittyDoc-305x305.png";s:5:"width";i:305;s:6:"height";i:305;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(7, 1, '_thumbnail_id', '6'),
(8, 1, '_edit_last', '1'),
(12, 8, '_wp_attached_file', '2013/11/logo.png'),
(11, 1, '_wp_old_slug', 'hello-world'),
(13, 8, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:461;s:6:"height";i:70;s:4:"file";s:16:"2013/11/logo.png";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:15:"logo-150x70.png";s:5:"width";i:150;s:6:"height";i:70;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:15:"logo-300x45.png";s:5:"width";i:300;s:6:"height";i:45;s:9:"mime-type";s:9:"image/png";}s:14:"post-thumbnail";a:4:{s:4:"file";s:15:"logo-305x46.png";s:5:"width";i:305;s:6:"height";i:46;s:9:"mime-type";s:9:"image/png";}s:10:"background";a:4:{s:4:"file";s:15:"logo-305x46.png";s:5:"width";i:305;s:6:"height";i:46;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(23, 10, '_menu_item_type', 'post_type'),
(24, 10, '_menu_item_menu_item_parent', '0'),
(25, 10, '_menu_item_object_id', '2'),
(26, 10, '_menu_item_object', 'page'),
(27, 10, '_menu_item_target', ''),
(28, 10, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(29, 10, '_menu_item_xfn', ''),
(30, 10, '_menu_item_url', ''),
(31, 10, '_menu_item_orphaned', '1384935944'),
(32, 11, '_edit_last', '1'),
(33, 11, '_edit_lock', '1387184777:1'),
(34, 13, '_wp_attached_file', '2013/12/screen568x5682.jpeg'),
(35, 13, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:320;s:6:"height";i:568;s:4:"file";s:27:"2013/12/screen568x5682.jpeg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:27:"screen568x5682-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:27:"screen568x5682-169x300.jpeg";s:5:"width";i:169;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(36, 14, '_wp_attached_file', '2013/12/screenn568x5681.jpeg'),
(37, 14, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:320;s:6:"height";i:568;s:4:"file";s:28:"2013/12/screenn568x5681.jpeg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:28:"screenn568x5681-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:28:"screenn568x5681-169x300.jpeg";s:5:"width";i:169;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(38, 15, '_edit_last', '1'),
(39, 15, '_edit_lock', '1387188821:1'),
(45, 12, '_edit_lock', '1387185401:1'),
(44, 12, '_edit_last', '1'),
(46, 19, '_wp_attached_file', '2013/12/1screen568x5682.jpeg'),
(47, 19, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:320;s:6:"height";i:568;s:4:"file";s:28:"2013/12/1screen568x5682.jpeg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:28:"1screen568x5682-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:28:"1screen568x5682-169x300.jpeg";s:5:"width";i:169;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(48, 20, '_wp_attached_file', '2013/12/screen568x5683.jpeg'),
(49, 20, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:320;s:6:"height";i:568;s:4:"file";s:27:"2013/12/screen568x5683.jpeg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:27:"screen568x5683-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:27:"screen568x5683-169x300.jpeg";s:5:"width";i:169;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(52, 12, '_thumbnail_id', '20'),
(53, 15, '_thumbnail_id', '13'),
(54, 22, '_edit_last', '1'),
(55, 22, '_edit_lock', '1387186100:1'),
(56, 23, '_wp_attached_file', '2013/12/11.jpg'),
(57, 23, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:186;s:6:"height";i:310;s:4:"file";s:14:"2013/12/11.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:14:"11-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:14:"11-180x300.jpg";s:5:"width";i:180;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(58, 24, '_wp_attached_file', '2013/12/1screen568x5683.jpeg'),
(59, 24, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:320;s:6:"height";i:568;s:4:"file";s:28:"2013/12/1screen568x5683.jpeg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:28:"1screen568x5683-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:28:"1screen568x5683-169x300.jpeg";s:5:"width";i:169;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(60, 25, '_wp_attached_file', '2013/12/unnamed3.jpg'),
(61, 25, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:480;s:6:"height";i:800;s:4:"file";s:20:"2013/12/unnamed3.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"unnamed3-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"unnamed3-180x300.jpg";s:5:"width";i:180;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(62, 22, '_thumbnail_id', '25'),
(66, 28, '_edit_lock', '1387790307:1'),
(65, 28, '_edit_last', '1'),
(70, 30, '_edit_lock', '1387788353:1'),
(69, 30, '_edit_last', '1'),
(74, 32, '_edit_lock', '1387787997:1'),
(73, 32, '_edit_last', '1'),
(78, 34, '_edit_lock', '1387186862:1'),
(77, 34, '_edit_last', '1'),
(79, 35, '_wp_attached_file', '2013/12/unnamed2.png'),
(80, 35, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:186;s:6:"height";i:310;s:4:"file";s:20:"2013/12/unnamed2.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"unnamed2-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:20:"unnamed2-180x300.png";s:5:"width";i:180;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(81, 36, '_wp_attached_file', '2013/12/unnamed12.png'),
(82, 36, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:186;s:6:"height";i:310;s:4:"file";s:21:"2013/12/unnamed12.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:21:"unnamed12-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:21:"unnamed12-180x300.png";s:5:"width";i:180;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(83, 37, '_wp_attached_file', '2013/12/unnamed22.jpg'),
(84, 37, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:186;s:6:"height";i:310;s:4:"file";s:21:"2013/12/unnamed22.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:21:"unnamed22-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:21:"unnamed22-180x300.jpg";s:5:"width";i:180;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(85, 34, '_thumbnail_id', '35'),
(89, 39, '_edit_lock', '1387188826:1'),
(88, 39, '_edit_last', '1'),
(90, 40, '_wp_attached_file', '2013/12/unnamed4.jpg'),
(91, 40, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:186;s:6:"height";i:310;s:4:"file";s:20:"2013/12/unnamed4.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"unnamed4-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"unnamed4-180x300.jpg";s:5:"width";i:180;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(92, 41, '_wp_attached_file', '2013/12/unnamed3.png'),
(93, 41, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:186;s:6:"height";i:310;s:4:"file";s:20:"2013/12/unnamed3.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"unnamed3-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:20:"unnamed3-180x300.png";s:5:"width";i:180;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(94, 42, '_wp_attached_file', '2013/12/unnamed23.jpg'),
(95, 42, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:186;s:6:"height";i:310;s:4:"file";s:21:"2013/12/unnamed23.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:21:"unnamed23-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:21:"unnamed23-180x300.jpg";s:5:"width";i:180;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(96, 39, '_thumbnail_id', '41'),
(100, 44, '_edit_lock', '1387187560:1'),
(99, 44, '_edit_last', '1'),
(103, 47, '_wp_attached_file', '2013/12/screen568x5684.jpeg'),
(104, 47, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:320;s:6:"height";i:568;s:4:"file";s:27:"2013/12/screen568x5684.jpeg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:27:"screen568x5684-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:27:"screen568x5684-169x300.jpeg";s:5:"width";i:169;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(105, 44, '_thumbnail_id', '47'),
(109, 49, '_edit_lock', '1387790144:1'),
(108, 49, '_edit_last', '1'),
(113, 51, '_edit_lock', '1387187846:1'),
(112, 51, '_edit_last', '1'),
(117, 53, '_edit_lock', '1387187963:1'),
(116, 53, '_edit_last', '1'),
(121, 55, '_edit_lock', '1387188088:1'),
(120, 55, '_edit_last', '1'),
(125, 57, '_edit_lock', '1387188187:1'),
(124, 57, '_edit_last', '1'),
(129, 59, '_edit_lock', '1387188309:1'),
(128, 59, '_edit_last', '1'),
(133, 61, '_edit_lock', '1387188412:1'),
(132, 61, '_edit_last', '1'),
(138, 65, '_wp_attached_file', '2013/12/logo.png'),
(139, 65, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:100;s:4:"file";s:16:"2013/12/logo.png";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:16:"logo-150x100.png";s:5:"width";i:150;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(140, 1, '_wp_trash_meta_status', 'publish'),
(141, 1, '_wp_trash_meta_time', '1387193941'),
(142, 1, '_wp_trash_meta_comments_status', 'a:1:{i:1;s:1:"1";}'),
(143, 66, '_wp_attached_file', '2013/12/logo1.png'),
(144, 66, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:100;s:4:"file";s:17:"2013/12/logo1.png";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"logo1-150x100.png";s:5:"width";i:150;s:6:"height";i:100;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(145, 61, '_wp_trash_meta_status', 'publish'),
(146, 61, '_wp_trash_meta_time', '1387772909'),
(147, 59, '_wp_trash_meta_status', 'publish'),
(148, 59, '_wp_trash_meta_time', '1387772910'),
(149, 57, '_wp_trash_meta_status', 'publish'),
(150, 57, '_wp_trash_meta_time', '1387772910'),
(151, 55, '_wp_trash_meta_status', 'publish'),
(152, 55, '_wp_trash_meta_time', '1387772910'),
(153, 53, '_wp_trash_meta_status', 'publish'),
(154, 53, '_wp_trash_meta_time', '1387772911'),
(155, 51, '_wp_trash_meta_status', 'publish'),
(156, 51, '_wp_trash_meta_time', '1387772911'),
(157, 68, '_wp_attached_file', '2013/12/ODS.png'),
(158, 68, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:541;s:6:"height";i:900;s:4:"file";s:15:"2013/12/ODS.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:15:"ODS-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:15:"ODS-180x300.png";s:5:"width";i:180;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(159, 69, '_wp_attached_file', '2013/12/ODS2.png'),
(160, 69, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:600;s:6:"height";i:900;s:4:"file";s:16:"2013/12/ODS2.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:16:"ODS2-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:16:"ODS2-200x300.png";s:5:"width";i:200;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(161, 70, '_wp_attached_file', '2013/12/ODS3.png'),
(162, 70, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:600;s:6:"height";i:900;s:4:"file";s:16:"2013/12/ODS3.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:16:"ODS3-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:16:"ODS3-200x300.png";s:5:"width";i:200;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(163, 32, '_thumbnail_id', '69'),
(166, 73, '_wp_attached_file', '2013/12/alarm.png'),
(167, 73, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:720;s:6:"height";i:1280;s:4:"file";s:17:"2013/12/alarm.png";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"alarm-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:17:"alarm-168x300.png";s:5:"width";i:168;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:18:"alarm-576x1024.png";s:5:"width";i:576;s:6:"height";i:1024;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(168, 74, '_wp_attached_file', '2013/12/notification.png'),
(169, 74, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:720;s:6:"height";i:1280;s:4:"file";s:24:"2013/12/notification.png";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"notification-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:24:"notification-168x300.png";s:5:"width";i:168;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:25:"notification-576x1024.png";s:5:"width";i:576;s:6:"height";i:1024;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(170, 75, '_wp_attached_file', '2013/12/search_location.png'),
(171, 75, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:720;s:6:"height";i:1280;s:4:"file";s:27:"2013/12/search_location.png";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:27:"search_location-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:27:"search_location-168x300.png";s:5:"width";i:168;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:28:"search_location-576x1024.png";s:5:"width";i:576;s:6:"height";i:1024;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(172, 76, '_wp_attached_file', '2013/12/set_position.png'),
(173, 76, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:720;s:6:"height";i:1280;s:4:"file";s:24:"2013/12/set_position.png";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"set_position-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:24:"set_position-168x300.png";s:5:"width";i:168;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:25:"set_position-576x1024.png";s:5:"width";i:576;s:6:"height";i:1024;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(174, 77, '_wp_attached_file', '2013/12/splash_screen.png'),
(175, 77, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:720;s:6:"height";i:1280;s:4:"file";s:25:"2013/12/splash_screen.png";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:25:"splash_screen-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:25:"splash_screen-168x300.png";s:5:"width";i:168;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:26:"splash_screen-576x1024.png";s:5:"width";i:576;s:6:"height";i:1024;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(176, 30, '_thumbnail_id', '77'),
(179, 80, '_wp_attached_file', '2013/12/add_server_ip.png'),
(180, 80, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1080;s:6:"height";i:1920;s:4:"file";s:25:"2013/12/add_server_ip.png";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:25:"add_server_ip-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:25:"add_server_ip-168x300.png";s:5:"width";i:168;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:26:"add_server_ip-576x1024.png";s:5:"width";i:576;s:6:"height";i:1024;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(181, 81, '_wp_attached_file', '2013/12/choose_canvas_color.png'),
(182, 81, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1080;s:6:"height";i:1920;s:4:"file";s:31:"2013/12/choose_canvas_color.png";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:31:"choose_canvas_color-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:31:"choose_canvas_color-168x300.png";s:5:"width";i:168;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:32:"choose_canvas_color-576x1024.png";s:5:"width";i:576;s:6:"height";i:1024;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(183, 82, '_wp_attached_file', '2013/12/choose_pen_size.png'),
(184, 82, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1080;s:6:"height";i:1920;s:4:"file";s:27:"2013/12/choose_pen_size.png";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:27:"choose_pen_size-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:27:"choose_pen_size-168x300.png";s:5:"width";i:168;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:28:"choose_pen_size-576x1024.png";s:5:"width";i:576;s:6:"height";i:1024;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(185, 83, '_wp_attached_file', '2013/12/splash_screen1.png'),
(186, 83, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:864;s:6:"height";i:1536;s:4:"file";s:26:"2013/12/splash_screen1.png";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:26:"splash_screen1-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:26:"splash_screen1-168x300.png";s:5:"width";i:168;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:27:"splash_screen1-576x1024.png";s:5:"width";i:576;s:6:"height";i:1024;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(205, 28, '_thumbnail_id', '92'),
(192, 87, '_wp_attached_file', '2013/12/timetag.png'),
(193, 87, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:286;s:6:"height";i:557;s:4:"file";s:19:"2013/12/timetag.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"timetag-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:19:"timetag-154x300.png";s:5:"width";i:154;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(194, 88, '_wp_attached_file', '2013/12/timetag2.png'),
(195, 88, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:285;s:6:"height";i:558;s:4:"file";s:20:"2013/12/timetag2.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"timetag2-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:20:"timetag2-153x300.png";s:5:"width";i:153;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(196, 89, '_wp_attached_file', '2013/12/timetag3.png'),
(197, 89, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:287;s:6:"height";i:560;s:4:"file";s:20:"2013/12/timetag3.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"timetag3-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:20:"timetag3-153x300.png";s:5:"width";i:153;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(198, 90, '_wp_attached_file', '2013/12/timetag4.png'),
(199, 90, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:285;s:6:"height";i:560;s:4:"file";s:20:"2013/12/timetag4.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"timetag4-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:20:"timetag4-152x300.png";s:5:"width";i:152;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}'),
(200, 49, '_thumbnail_id', '87'),
(203, 92, '_wp_attached_file', '2013/12/splash_screen2.png'),
(204, 92, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:817;s:6:"height";i:1448;s:4:"file";s:26:"2013/12/splash_screen2.png";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:26:"splash_screen2-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:26:"splash_screen2-169x300.png";s:5:"width";i:169;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:27:"splash_screen2-577x1024.png";s:5:"width";i:577;s:6:"height";i:1024;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:10:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";}}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_posts`
--

CREATE TABLE IF NOT EXISTS `wp_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext NOT NULL,
  `post_title` text NOT NULL,
  `post_excerpt` text NOT NULL,
  `post_status` varchar(20) NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) NOT NULL DEFAULT 'open',
  `post_password` varchar(20) NOT NULL DEFAULT '',
  `post_name` varchar(200) NOT NULL DEFAULT '',
  `to_ping` text NOT NULL,
  `pinged` text NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=94 ;

--
-- Dumping data for table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2013-10-29 15:43:53', '2013-10-29 15:43:53', '', 'WittyDoc', '', 'trash', 'open', 'open', '', 'witty-doc', '', '', '2013-12-16 11:39:01', '2013-12-16 11:39:01', '', 0, 'http://mobile.antking.net/?p=1', 0, 'post', '', 1),
(7, 1, '2013-11-20 08:20:22', '2013-11-20 08:20:22', '', 'WittyDoc', '', 'inherit', 'open', 'open', '', '1-revision-v1', '', '', '2013-11-20 08:20:22', '2013-11-20 08:20:22', '', 1, 'http://mobile.antking.net/2013/11/20/1-revision-v1/', 0, 'revision', '', 0),
(2, 1, '2013-10-29 15:43:53', '2013-10-29 15:43:53', 'This is an example page. It''s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I''m a bike messenger by day, aspiring actor by night, and this is my blog. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin'' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href="mobile.antking.net/wp-admin/">your dashboard</a> to delete this page and create new pages for your content. Have fun!', 'Sample Page', '', 'publish', 'open', 'open', '', 'sample-page', '', '', '2013-10-29 15:43:53', '2013-10-29 15:43:53', '', 0, 'http://mobile.antking.net/?page_id=2', 0, 'page', '', 0),
(12, 1, '2013-12-16 09:16:38', '2013-12-16 09:16:38', 'Features:\r\n1. Show off your ride.\r\n2. Create a virtual garage for your rides with details vehicle profiles.\r\n3. Discover amazing new rides from around the world and connect with people who share.\r\n4. Check out the newest autos.\r\n5. Write comment/like autos and share to your Facebook wall .\r\nTechnologies:\r\n1. iPhone SDK\r\n2. Parse SDK, Cloud Made SDK\r\n\r\nGo to site: <a href="http://itunes.com/app/autoprofilez">Link here</a>\r\n\r\n<a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/1screen568x5682.jpeg"><img class="alignnone size-medium wp-image-19" alt="1screen568x568" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/1screen568x5682-169x300.jpeg" width="169" height="300" /></a>       <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/screen568x5683.jpeg"><img class="alignnone size-medium wp-image-20" alt="screen568x568" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/screen568x5683-169x300.jpeg" width="169" height="300" /></a>', 'AutoprofileZ - iPhone', '', 'publish', 'open', 'open', '', 'autoprofilez-iphone', '', '', '2013-12-16 09:16:38', '2013-12-16 09:16:38', '', 0, 'http://localhost.mobileantking.com/?p=12', 1, 'post', '', 0),
(5, 1, '2013-11-20 08:19:48', '2013-11-20 08:19:48', '', 'WittyDoc', '', 'inherit', 'open', 'open', '', '1-autosave-v1', '', '', '2013-11-20 08:19:48', '2013-11-20 08:19:48', '', 1, 'http://mobile.antking.net/2013/11/20/1-autosave-v1/', 0, 'revision', '', 0),
(6, 1, '2013-11-20 08:19:29', '2013-11-20 08:19:29', '', 'WittyDoc', '', 'inherit', 'open', 'open', '', 'wittydoc', '', '', '2013-11-20 08:19:29', '2013-11-20 08:19:29', '', 1, 'http://mobile.antking.net/wp-content/uploads/2013/10/WittyDoc.png', 0, 'attachment', 'image/png', 0),
(8, 1, '2013-11-20 08:23:56', '2013-11-20 08:23:56', '', 'logo', '', 'inherit', 'open', 'open', '', 'logo', '', '', '2013-11-20 08:23:56', '2013-11-20 08:23:56', '', 0, 'http://mobile.antking.net/wp-content/uploads/2013/11/logo.png', 0, 'attachment', 'image/png', 0),
(10, 1, '2013-11-20 08:25:44', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'open', 'open', '', '', '', '', '2013-11-20 08:25:44', '0000-00-00 00:00:00', '', 0, 'http://mobile.antking.net/?p=10', 1, 'nav_menu_item', '', 0),
(11, 1, '2013-12-16 09:06:17', '0000-00-00 00:00:00', 'Features:\n1. View and book KFC fast food\n2. Push notifications\n3. View KFC store on Maps\nTechnologies:\n1. Android &amp; iPhone SDK\n2. Apple Push notification server and Android Google Push Message with PHP server\n\n<a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/screen568x5682.jpeg"><img class="alignnone size-medium wp-image-13" alt="screen568x568" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/screen568x5682-169x300.jpeg" width="169" height="300" /></a>      <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/screenn568x5681.jpeg"><img class="alignnone size-medium wp-image-14" alt="screenn568x568" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/screenn568x5681-169x300.jpeg" width="169" height="300" /></a>', 'KFC Vietnam - iPhone, Android', '', 'draft', 'open', 'open', '', 'httpsitunes-apple-comtrappkfc-vnid562164615mt8', '', '', '2013-12-16 09:06:17', '2013-12-16 09:06:17', '', 0, 'http://localhost.mobileantking.com/?p=11', 10, 'post', '', 0),
(13, 1, '2013-12-16 09:04:42', '2013-12-16 09:04:42', '', 'screen568x568', '', 'inherit', 'open', 'open', '', 'screen568x568', '', '', '2013-12-16 09:04:42', '2013-12-16 09:04:42', '', 11, 'http://localhost.mobileantking.com/wp-content/uploads/2013/12/screen568x5682.jpeg', 0, 'attachment', 'image/jpeg', 0),
(14, 1, '2013-12-16 09:04:43', '2013-12-16 09:04:43', '', 'screenn568x568', '', 'inherit', 'open', 'open', '', 'screenn568x568', '', '', '2013-12-16 09:04:43', '2013-12-16 09:04:43', '', 11, 'http://localhost.mobileantking.com/wp-content/uploads/2013/12/screenn568x5681.jpeg', 0, 'attachment', 'image/jpeg', 0),
(15, 1, '2013-12-16 09:06:58', '2013-12-16 09:06:58', 'Features:\r\n1. View and book KFC fast food\r\n2. Push notifications\r\n3. View KFC store on Maps\r\nTechnologies:\r\n1. Android &amp; iPhone SDK\r\n2. Apple Push notification server and Android Google Push Message with PHP server\r\n\r\nGo to site: <a href="https://itunes.apple.com/tr/app/kfc-vn/id562164615?mt=8">Link here</a>\r\n<a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/screen568x5682.jpeg"><img alt="screen568x568" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/screen568x5682-169x300.jpeg" width="169" height="300" /></a>      <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/screenn568x5681.jpeg"><img alt="screenn568x568" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/screenn568x5681-169x300.jpeg" width="169" height="300" /></a>', 'KFC Vietnam - iPhone, Android', '', 'publish', 'open', 'open', '', 'kfc-vietnam-iphone-android', '', '', '2013-12-16 09:12:32', '2013-12-16 09:12:32', '', 0, 'http://localhost.mobileantking.com/?p=15', 0, 'post', '', 0),
(16, 1, '2013-12-16 09:06:58', '2013-12-16 09:06:58', 'Features:\r\n1. View and book KFC fast food\r\n2. Push notifications\r\n3. View KFC store on Maps\r\nTechnologies:\r\n1. Android &amp; iPhone SDK\r\n2. Apple Push notification server and Android Google Push Message with PHP server\r\n\r\n<a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/screen568x5682.jpeg"><img alt="screen568x568" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/screen568x5682-169x300.jpeg" width="169" height="300" /></a>      <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/screenn568x5681.jpeg"><img alt="screenn568x568" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/screenn568x5681-169x300.jpeg" width="169" height="300" /></a>', 'KFC Vietnam - iPhone, Android', '', 'inherit', 'open', 'open', '', '15-revision-v1', '', '', '2013-12-16 09:06:58', '2013-12-16 09:06:58', '', 15, 'http://localhost.mobileantking.com/2013/12/16/15-revision-v1/', 0, 'revision', '', 0),
(17, 1, '2013-12-16 09:12:04', '2013-12-16 09:12:04', 'Features:\n1. View and book KFC fast food\n2. Push notifications\n3. View KFC store on Maps\nTechnologies:\n1. Android &amp; iPhone SDK\n2. Apple Push notification server and Android Google Push Message with PHP server\n\nGo to site: <a href="https://itunes.apple.com/tr/app/kfc-vn/id562164615?mt=8">Link here</a>\n<a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/screen568x5682.jpeg"><img alt="screen568x568" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/screen568x5682-169x300.jpeg" width="169" height="300" /></a>      <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/screenn568x5681.jpeg"><img alt="screenn568x568" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/screenn568x5681-169x300.jpeg" width="169" height="300" /></a>', 'KFC Vietnam - iPhone, Android', '', 'inherit', 'open', 'open', '', '15-autosave-v1', '', '', '2013-12-16 09:12:04', '2013-12-16 09:12:04', '', 15, 'http://localhost.mobileantking.com/2013/12/16/15-autosave-v1/', 0, 'revision', '', 0),
(18, 1, '2013-12-16 09:12:32', '2013-12-16 09:12:32', 'Features:\r\n1. View and book KFC fast food\r\n2. Push notifications\r\n3. View KFC store on Maps\r\nTechnologies:\r\n1. Android &amp; iPhone SDK\r\n2. Apple Push notification server and Android Google Push Message with PHP server\r\n\r\nGo to site: <a href="https://itunes.apple.com/tr/app/kfc-vn/id562164615?mt=8">Link here</a>\r\n<a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/screen568x5682.jpeg"><img alt="screen568x568" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/screen568x5682-169x300.jpeg" width="169" height="300" /></a>      <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/screenn568x5681.jpeg"><img alt="screenn568x568" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/screenn568x5681-169x300.jpeg" width="169" height="300" /></a>', 'KFC Vietnam - iPhone, Android', '', 'inherit', 'open', 'open', '', '15-revision-v1', '', '', '2013-12-16 09:12:32', '2013-12-16 09:12:32', '', 15, 'http://localhost.mobileantking.com/2013/12/16/15-revision-v1/', 0, 'revision', '', 0),
(19, 1, '2013-12-16 09:13:26', '2013-12-16 09:13:26', '', '1screen568x568', '', 'inherit', 'open', 'open', '', '1screen568x568', '', '', '2013-12-16 09:13:26', '2013-12-16 09:13:26', '', 12, 'http://localhost.mobileantking.com/wp-content/uploads/2013/12/1screen568x5682.jpeg', 0, 'attachment', 'image/jpeg', 0),
(20, 1, '2013-12-16 09:13:27', '2013-12-16 09:13:27', '', 'screen568x568', '', 'inherit', 'open', 'open', '', 'screen568x568-2', '', '', '2013-12-16 09:13:27', '2013-12-16 09:13:27', '', 12, 'http://localhost.mobileantking.com/wp-content/uploads/2013/12/screen568x5683.jpeg', 0, 'attachment', 'image/jpeg', 0),
(21, 1, '2013-12-16 09:16:38', '2013-12-16 09:16:38', 'Features:\r\n1. Show off your ride.\r\n2. Create a virtual garage for your rides with details vehicle profiles.\r\n3. Discover amazing new rides from around the world and connect with people who share.\r\n4. Check out the newest autos.\r\n5. Write comment/like autos and share to your Facebook wall .\r\nTechnologies:\r\n1. iPhone SDK\r\n2. Parse SDK, Cloud Made SDK\r\n\r\nGo to site: <a href="http://itunes.com/app/autoprofilez">Link here</a>\r\n\r\n<a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/1screen568x5682.jpeg"><img class="alignnone size-medium wp-image-19" alt="1screen568x568" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/1screen568x5682-169x300.jpeg" width="169" height="300" /></a>       <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/screen568x5683.jpeg"><img class="alignnone size-medium wp-image-20" alt="screen568x568" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/screen568x5683-169x300.jpeg" width="169" height="300" /></a>', 'AutoprofileZ - iPhone', '', 'inherit', 'open', 'open', '', '12-revision-v1', '', '', '2013-12-16 09:16:38', '2013-12-16 09:16:38', '', 12, 'http://localhost.mobileantking.com/2013/12/16/12-revision-v1/', 0, 'revision', '', 0),
(22, 1, '2013-12-16 09:28:18', '2013-12-16 09:28:18', 'Features:\r\n1. Tracking vehicles on Maps\r\n2. Searching with vehicle name or location\r\n3. Support English &amp; Vietnamese\r\nTechnologies:\r\n1. Android &amp; iPhone SDK\r\n2. Cloud Made SDK\r\n\r\nDownload on Google Play:  <a href="https://play.google.com/store/apps/details?id=vinasourcecode.outsource.fireants ">Link here</a>\r\non Itunes:  <a href="https://itunes.apple.com/sg/app/fireants/id417411165?mt=8">FireAnts version 1.08</a>   or   <a href="https://itunes.apple.com/sg/app/fireants-hd/id529128929?mt=8">FireAnts HD version 1.0.3</a>\r\n\r\n&nbsp;\r\n\r\n<a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed3.jpg"><img class="alignnone size-medium wp-image-25" alt="unnamed" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed3-180x300.jpg" width="180" height="300" />       <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/1screen568x5683.jpeg"><img alt="1screen568x568" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/1screen568x5683-169x300.jpeg" width="169" height="300" /></a> </a>       <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/11.jpg"><img alt="1" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/11-180x300.jpg" width="180" height="300" /></a>', 'FireAnts - iPad, iPhone, Android', '', 'publish', 'open', 'open', '', 'fireants-ipad-iphone-android', '', '', '2013-12-16 09:28:18', '2013-12-16 09:28:18', '', 0, 'http://localhost.mobileantking.com/?p=22', 2, 'post', '', 0),
(23, 1, '2013-12-16 09:27:11', '2013-12-16 09:27:11', '', '1', '', 'inherit', 'open', 'open', '', '1', '', '', '2013-12-16 09:27:11', '2013-12-16 09:27:11', '', 22, 'http://localhost.mobileantking.com/wp-content/uploads/2013/12/11.jpg', 0, 'attachment', 'image/jpeg', 0),
(24, 1, '2013-12-16 09:27:12', '2013-12-16 09:27:12', '', '1screen568x568', '', 'inherit', 'open', 'open', '', '1screen568x568-2', '', '', '2013-12-16 09:27:12', '2013-12-16 09:27:12', '', 22, 'http://localhost.mobileantking.com/wp-content/uploads/2013/12/1screen568x5683.jpeg', 0, 'attachment', 'image/jpeg', 0),
(25, 1, '2013-12-16 09:27:13', '2013-12-16 09:27:13', '', 'unnamed', '', 'inherit', 'open', 'open', '', 'unnamed', '', '', '2013-12-16 09:27:13', '2013-12-16 09:27:13', '', 22, 'http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed3.jpg', 0, 'attachment', 'image/jpeg', 0),
(26, 1, '2013-12-16 09:28:18', '2013-12-16 09:28:18', 'Features:\r\n1. Tracking vehicles on Maps\r\n2. Searching with vehicle name or location\r\n3. Support English &amp; Vietnamese\r\nTechnologies:\r\n1. Android &amp; iPhone SDK\r\n2. Cloud Made SDK\r\n\r\nDownload on Google Play:  <a href="https://play.google.com/store/apps/details?id=vinasourcecode.outsource.fireants ">Link here</a>\r\non Itunes:  <a href="https://itunes.apple.com/sg/app/fireants/id417411165?mt=8">FireAnts version 1.08</a>   or   <a href="https://itunes.apple.com/sg/app/fireants-hd/id529128929?mt=8">FireAnts HD version 1.0.3</a>\r\n\r\n&nbsp;\r\n\r\n<a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed3.jpg"><img class="alignnone size-medium wp-image-25" alt="unnamed" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed3-180x300.jpg" width="180" height="300" />       <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/1screen568x5683.jpeg"><img alt="1screen568x568" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/1screen568x5683-169x300.jpeg" width="169" height="300" /></a> </a>       <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/11.jpg"><img alt="1" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/11-180x300.jpg" width="180" height="300" /></a>', 'FireAnts - iPad, iPhone, Android', '', 'inherit', 'open', 'open', '', '22-revision-v1', '', '', '2013-12-16 09:28:18', '2013-12-16 09:28:18', '', 22, 'http://localhost.mobileantking.com/2013/12/16/22-revision-v1/', 0, 'revision', '', 0),
(27, 1, '2013-12-16 09:29:22', '2013-12-16 09:29:22', 'Features:\n1. Tracking vehicles on Maps\n2. Searching with vehicle name or location\n3. Support English &amp; Vietnamese\nTechnologies:\n1. Android &amp; iPhone SDK\n2. Cloud Made SDK\n\nDownload on Google Play:  <a href="https://play.google.com/store/apps/details?id=vinasourcecode.outsource.fireants ">Link here</a>\non Itunes:  <a href="https://itunes.apple.com/sg/app/fireants/id417411165?mt=8">FireAnts version 1.08</a>   or   <a href="https://itunes.apple.com/sg/app/fireants-hd/id529128929?mt=8">FireAnts HD version 1.0.3</a>\n\n&nbsp;\n\n<a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed3.jpg"><img class="alignnone size-medium wp-image-25" alt="unnamed" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed3-180x300.jpg" width="180" height="300" />       </a><a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/1screen568x5683.jpeg"><img alt="1screen568x568" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/1screen568x5683-169x300.jpeg" width="169" height="300" /></a>        <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/11.jpg"><img alt="1" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/11-180x300.jpg" width="180" height="300" /></a>', 'FireAnts - iPad, iPhone, Android', '', 'inherit', 'open', 'open', '', '22-autosave-v1', '', '', '2013-12-16 09:29:22', '2013-12-16 09:29:22', '', 22, 'http://localhost.mobileantking.com/2013/12/16/22-autosave-v1/', 0, 'revision', '', 0),
(28, 1, '2013-12-16 09:31:20', '2013-12-16 09:31:20', 'Features:\r\n1. Draw something on canvas\r\n2. Save image file by proximity sensor\r\n3. Send image file to server through TCP/IP socket\r\nTechnologies:\r\n1. Android &amp; iPhone SDK\r\n2. Use canvas to draw\r\n3. Create TCP/IP socket to transfer file\r\n4. Use proximity sensor to detect and save drawings\r\n\r\n&nbsp;\r\n\r\n<a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/splash_screen2.png"><img class="alignnone size-medium wp-image-92" alt="splash_screen" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/splash_screen2-169x300.png" width="169" height="300" /></a>   <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/add_server_ip.png"><img class="alignnone size-medium wp-image-80" alt="add_server_ip" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/add_server_ip-168x300.png" width="168" height="300" /></a>    <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/choose_canvas_color.png"><img class="alignnone size-medium wp-image-81" alt="choose_canvas_color" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/choose_canvas_color-168x300.png" width="168" height="300" /></a>    <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/choose_pen_size.png"><img class="alignnone size-medium wp-image-82" alt="choose_pen_size" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/choose_pen_size-168x300.png" width="168" height="300" /></a>', 'DrawingApps - Android', '', 'publish', 'open', 'open', '', 'drawingapps-android', '', '', '2013-12-23 09:18:19', '2013-12-23 09:18:19', '', 0, 'http://localhost.mobileantking.com/?p=28', 3, 'post', '', 0),
(29, 1, '2013-12-16 09:31:20', '2013-12-16 09:31:20', 'Features:\r\n1. Draw something on canvas\r\n2. Save image file by proximity sensor\r\n3. Send image file to server through TCP/IP socket\r\nTechnologies:\r\n1. Android &amp; iPhone SDK\r\n2. Use canvas to draw\r\n3. Create TCP/IP socket to transfer file\r\n4. Use proximity sensor to detect and save drawings\r\n\r\n&nbsp;\r\n\r\n&nbsp;', 'DrawingApps - Android', '', 'inherit', 'open', 'open', '', '28-revision-v1', '', '', '2013-12-16 09:31:20', '2013-12-16 09:31:20', '', 28, 'http://localhost.mobileantking.com/2013/12/16/28-revision-v1/', 0, 'revision', '', 0),
(30, 1, '2013-12-16 09:33:32', '2013-12-16 09:33:32', '<p style="text-align: left;">Features:\r\n1. Choose position on map to alarm\r\n2. Auto check when user''s phone within radius of Choosed position to alarm\r\n3. Share through Facebook, Twitter, Google+\r\nTechnologies:\r\n1. Android, iPhone &amp; WP8 SDK\r\n2. Broadcast Receiver to alarm\r\n3. Cloud Made SDK</p>\r\nGo to site : <a href="http://puddy.covexis.com">puddy.covexis.com</a>\r\n\r\n&nbsp;\r\n\r\n<a href="http://puddy.covexis.com"><a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/alarm.png"><a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/splash_screen.png"><img alt="splash_screen" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/splash_screen-168x300.png" width="168" height="300" /></a>     </a> <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/notification.png"><img class="alignnone size-medium wp-image-74" alt="notification" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/notification-168x300.png" width="168" height="300" /></a>     <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/search_location.png"><img class="alignnone size-medium wp-image-75" alt="search_location" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/search_location-168x300.png" width="168" height="300" /></a>      <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/set_position.png"><img class="alignnone size-medium wp-image-76" alt="set_position" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/set_position-168x300.png" width="168" height="300" /></a> </a>\r\n\r\n&nbsp;', 'Puddy - iPhone, Android, WP8', '', 'publish', 'open', 'open', '', 'puddy-iphone-android-wp8', '', '', '2013-12-23 08:45:47', '2013-12-23 08:45:47', '', 0, 'http://localhost.mobileantking.com/?p=30', 9, 'post', '', 0),
(31, 1, '2013-12-16 09:33:32', '2013-12-16 09:33:32', 'Features:\r\n1. Choose position on map to alarm\r\n2. Auto check when user''s phone within radius of Choosed position to alarm\r\n3. Share through Facebook, Twitter, Google+\r\nTechnologies:\r\n1. Android, iPhone &amp; WP8 SDK\r\n2. Broadcast Receiver to alarm\r\n3. Cloud Made SDK\r\n\r\nGo to site : <a href="http://puddy.covexis.com">puddy.covexis.com</a>\r\n\r\n', 'Puddy - iPhone, Android, WP8', '', 'inherit', 'open', 'open', '', '30-revision-v1', '', '', '2013-12-16 09:33:32', '2013-12-16 09:33:32', '', 30, 'http://localhost.mobileantking.com/2013/12/16/30-revision-v1/', 0, 'revision', '', 0),
(32, 1, '2013-12-16 09:35:17', '2013-12-16 09:35:17', 'Features:\r\n1. Tracking device on map\r\n2. Push notification\r\n3. View messages about devices\r\n4. Add new device\r\n5. Register new account\r\n6. Setting for device\r\n7. Share through FB, Twitter, Google+,...\r\nTechnologies:\r\n1. Android, iPhone and WP8 SDK\r\n2. Cloud Made SDK\r\n3. Push notification for Android, iOS\r\n4. Scan QR code\r\n5. Parse Web service to get data\r\n6. Animation for left or right swiping\r\n\r\nGo to site: <a href="http://ods.covexis.com/">ods.covexis.com</a>\r\n\r\n<a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/ODS2.png"><img alt="ODS2" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/ODS2-200x300.png" width="200" height="300" /></a>    <img alt="ODS" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/ODS-180x300.png" width="180" height="300" />    <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/ODS3.png"><img alt="ODS3" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/ODS3-200x300.png" width="200" height="300" /></a>', 'ODS-Android, iPhone, WP8', '', 'publish', 'open', 'open', '', 'ods-android-iphone-wp8', '', '', '2013-12-23 08:39:52', '2013-12-23 08:39:52', '', 0, 'http://localhost.mobileantking.com/?p=32', 4, 'post', '', 0),
(33, 1, '2013-12-16 09:35:17', '2013-12-16 09:35:17', 'Features:\r\n1. Tracking device on map\r\n2. Push notification\r\n3. View messages about devices\r\n4. Add new device\r\n5. Register new account\r\n6. Setting for device\r\n7. Share through FB, Twitter, Google+,...\r\nTechnologies:\r\n1. Android, iPhone and WP8 SDK\r\n2. Cloud Made SDK\r\n3. Push notification for Android, iOS\r\n4. Scan QR code\r\n5. Parse Web service to get data\r\n6. Animation for left or right swiping \r\n\r\nGo to site: <a href="http://ods.covexis.com/">ods.covexis.com</a>', 'ODS-Android, iPhone, WP8', '', 'inherit', 'open', 'open', '', '32-revision-v1', '', '', '2013-12-16 09:35:17', '2013-12-16 09:35:17', '', 32, 'http://localhost.mobileantking.com/2013/12/16/32-revision-v1/', 0, 'revision', '', 0),
(34, 1, '2013-12-16 09:40:59', '2013-12-16 09:40:59', 'Technologies:\r\n1. Android SDK\r\n2. Google Maps APIs\r\n\r\nDownload on Google Play: <a href="https://play.google.com/store/apps/details?id=com.covexis.ulocate&amp;hl=en ">Link here</a>\r\n\r\n<a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed2.png"><img class="alignnone size-medium wp-image-35" alt="unnamed" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed2-180x300.png" width="180" height="300" /></a>      <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed12.png"><img class="alignnone size-medium wp-image-36" alt="unnamed1" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed12-180x300.png" width="180" height="300" /></a>       <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed22.jpg"><img class="alignnone size-medium wp-image-37" alt="unnamed2" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed22-180x300.jpg" width="180" height="300" /></a>', 'Ulocate - Android', '', 'publish', 'open', 'open', '', 'ulocate-android', '', '', '2013-12-16 09:40:59', '2013-12-16 09:40:59', '', 0, 'http://localhost.mobileantking.com/?p=34', 5, 'post', '', 0),
(35, 1, '2013-12-16 09:39:55', '2013-12-16 09:39:55', '', 'unnamed', '', 'inherit', 'open', 'open', '', 'unnamed-2', '', '', '2013-12-16 09:39:55', '2013-12-16 09:39:55', '', 34, 'http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed2.png', 0, 'attachment', 'image/png', 0),
(36, 1, '2013-12-16 09:39:56', '2013-12-16 09:39:56', '', 'unnamed1', '', 'inherit', 'open', 'open', '', 'unnamed1', '', '', '2013-12-16 09:39:56', '2013-12-16 09:39:56', '', 34, 'http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed12.png', 0, 'attachment', 'image/png', 0),
(37, 1, '2013-12-16 09:39:57', '2013-12-16 09:39:57', '', 'unnamed2', '', 'inherit', 'open', 'open', '', 'unnamed2', '', '', '2013-12-16 09:39:57', '2013-12-16 09:39:57', '', 34, 'http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed22.jpg', 0, 'attachment', 'image/jpeg', 0),
(38, 1, '2013-12-16 09:40:59', '2013-12-16 09:40:59', 'Technologies:\r\n1. Android SDK\r\n2. Google Maps APIs\r\n\r\nDownload on Google Play: <a href="https://play.google.com/store/apps/details?id=com.covexis.ulocate&amp;hl=en ">Link here</a>\r\n\r\n<a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed2.png"><img class="alignnone size-medium wp-image-35" alt="unnamed" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed2-180x300.png" width="180" height="300" /></a>      <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed12.png"><img class="alignnone size-medium wp-image-36" alt="unnamed1" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed12-180x300.png" width="180" height="300" /></a>       <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed22.jpg"><img class="alignnone size-medium wp-image-37" alt="unnamed2" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed22-180x300.jpg" width="180" height="300" /></a>', 'Ulocate - Android', '', 'inherit', 'open', 'open', '', '34-revision-v1', '', '', '2013-12-16 09:40:59', '2013-12-16 09:40:59', '', 34, 'http://localhost.mobileantking.com/2013/12/16/34-revision-v1/', 0, 'revision', '', 0),
(39, 1, '2013-12-16 09:45:40', '2013-12-16 09:45:40', 'Technologies:\r\n1. Android SDK\r\n2. Integrate library to get weather of yahoo\r\n3. Get current location\r\n4. Google Maps APIs\r\n\r\nDownload on Google Play:  <a href="https://play.google.com/store/apps/details?id=com.uglii&amp;hl=en">Link here</a>\r\n\r\n<a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed3.png"><img class="alignnone size-medium wp-image-41" alt="unnamed" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed3-180x300.png" width="180" height="300" /></a>      <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed4.jpg"><img alt="unnamed" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed4-180x300.jpg" width="180" height="300" /></a>       <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed23.jpg"><img class="alignnone size-medium wp-image-42" alt="unnamed2" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed23-180x300.jpg" width="180" height="300" /></a>', 'Ugli-Pre - Android, iPhone', '', 'publish', 'open', 'open', '', 'ugli-pre-android-iphone', '', '', '2013-12-16 10:08:22', '2013-12-16 10:08:22', '', 0, 'http://localhost.mobileantking.com/?p=39', 8, 'post', '', 0),
(40, 1, '2013-12-16 09:43:25', '2013-12-16 09:43:25', '', 'unnamed', '', 'inherit', 'open', 'open', '', 'unnamed-3', '', '', '2013-12-16 09:43:25', '2013-12-16 09:43:25', '', 39, 'http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed4.jpg', 0, 'attachment', 'image/jpeg', 0),
(41, 1, '2013-12-16 09:43:26', '2013-12-16 09:43:26', '', 'unnamed', '', 'inherit', 'open', 'open', '', 'unnamed-4', '', '', '2013-12-16 09:43:26', '2013-12-16 09:43:26', '', 39, 'http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed3.png', 0, 'attachment', 'image/png', 0),
(42, 1, '2013-12-16 09:43:27', '2013-12-16 09:43:27', '', 'unnamed2', '', 'inherit', 'open', 'open', '', 'unnamed2-2', '', '', '2013-12-16 09:43:27', '2013-12-16 09:43:27', '', 39, 'http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed23.jpg', 0, 'attachment', 'image/jpeg', 0),
(63, 1, '2013-12-16 10:08:22', '2013-12-16 10:08:22', 'Technologies:\r\n1. Android SDK\r\n2. Integrate library to get weather of yahoo\r\n3. Get current location\r\n4. Google Maps APIs\r\n\r\nDownload on Google Play:  <a href="https://play.google.com/store/apps/details?id=com.uglii&amp;hl=en">Link here</a>\r\n\r\n<a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed3.png"><img class="alignnone size-medium wp-image-41" alt="unnamed" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed3-180x300.png" width="180" height="300" /></a>      <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed4.jpg"><img alt="unnamed" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed4-180x300.jpg" width="180" height="300" /></a>       <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed23.jpg"><img class="alignnone size-medium wp-image-42" alt="unnamed2" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed23-180x300.jpg" width="180" height="300" /></a>', 'Ugli-Pre - Android, iPhone', '', 'inherit', 'open', 'open', '', '39-revision-v1', '', '', '2013-12-16 10:08:22', '2013-12-16 10:08:22', '', 39, 'http://localhost.mobileantking.com/2013/12/16/39-revision-v1/', 0, 'revision', '', 0),
(43, 1, '2013-12-16 09:45:40', '2013-12-16 09:45:40', 'Technologies:\r\n1. Android SDK\r\n2. Integrate library to get weather of yahoo\r\n3. Get current location\r\n4. Google Maps APIs\r\n\r\nDownload on Google Play: &lt;a href="<a href="https://play.google.com/store/apps/details?id=com.uglii&amp;hl=en">https://play.google.com/store/apps/details?id=com.uglii&amp;hl=en</a>"&gt;Link here&lt;/a&gt;\r\n\r\n<a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed3.png"><img class="alignnone size-medium wp-image-41" alt="unnamed" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed3-180x300.png" width="180" height="300" /></a>      <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed4.jpg"><img alt="unnamed" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed4-180x300.jpg" width="180" height="300" /></a>       <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed23.jpg"><img class="alignnone size-medium wp-image-42" alt="unnamed2" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/unnamed23-180x300.jpg" width="180" height="300" /></a>', 'Ugli-Pre - Android, iPhone', '', 'inherit', 'open', 'open', '', '39-revision-v1', '', '', '2013-12-16 09:45:40', '2013-12-16 09:45:40', '', 39, 'http://localhost.mobileantking.com/2013/12/16/39-revision-v1/', 0, 'revision', '', 0),
(44, 1, '2013-12-16 09:50:06', '2013-12-16 09:50:06', 'Technologies:\r\n1. Android &amp; iOS SDK\r\n2. QRCode scaner and generator framework\r\n\r\nDownload on Google Play: <a href="https://play.google.com/store/apps/details?id=com.wittydoc">Link here</a>\r\n\r\non Itunes: <a href="https://itunes.apple.com/us/app/wittydoc/id613954583">Link here</a>\r\n\r\n<a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/screen568x5684.jpeg"><img class="alignnone size-medium wp-image-47" alt="screen568x568" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/screen568x5684-169x300.jpeg" width="169" height="300" /></a>', 'WittyDocs - Android, iPhone', '', 'publish', 'open', 'open', '', 'wittydocs-android-iphone', '', '', '2013-12-16 09:52:38', '2013-12-16 09:52:38', '', 0, 'http://localhost.mobileantking.com/?p=44', 7, 'post', '', 0),
(45, 1, '2013-12-16 09:50:06', '2013-12-16 09:50:06', 'Technologies:\r\n1. Android &amp; iOS SDK\r\n2. QRCode scaner and generator framework\r\n\r\nDownload on Google Play: <a href="https://play.google.com/store/apps/details?id=com.wittydoc">Link here</a>\r\n\r\non Itunes: <a href="https://itunes.apple.com/us/app/wittydoc/id613954583">Link here</a>\r\n\r\n', 'WittyDocs - Android, iPhone', '', 'inherit', 'open', 'open', '', '44-revision-v1', '', '', '2013-12-16 09:50:06', '2013-12-16 09:50:06', '', 44, 'http://localhost.mobileantking.com/2013/12/16/44-revision-v1/', 0, 'revision', '', 0),
(46, 1, '2013-12-16 09:52:10', '2013-12-16 09:52:10', 'Technologies:\n1. Android &amp; iOS SDK\n2. QRCode scaner and generator framework\n\nDownload on Google Play: <a href="https://play.google.com/store/apps/details?id=com.wittydoc">Link here</a>\n\non Itunes: <a href="https://itunes.apple.com/us/app/wittydoc/id613954583">Link here</a>\n\n\n\n', 'WittyDocs - Android, iPhone', '', 'inherit', 'open', 'open', '', '44-autosave-v1', '', '', '2013-12-16 09:52:10', '2013-12-16 09:52:10', '', 44, 'http://localhost.mobileantking.com/2013/12/16/44-autosave-v1/', 0, 'revision', '', 0),
(47, 1, '2013-12-16 09:52:20', '2013-12-16 09:52:20', '', 'screen568x568', '', 'inherit', 'open', 'open', '', 'screen568x568-3', '', '', '2013-12-16 09:52:20', '2013-12-16 09:52:20', '', 44, 'http://localhost.mobileantking.com/wp-content/uploads/2013/12/screen568x5684.jpeg', 0, 'attachment', 'image/jpeg', 0),
(48, 1, '2013-12-16 09:52:38', '2013-12-16 09:52:38', 'Technologies:\r\n1. Android &amp; iOS SDK\r\n2. QRCode scaner and generator framework\r\n\r\nDownload on Google Play: <a href="https://play.google.com/store/apps/details?id=com.wittydoc">Link here</a>\r\n\r\non Itunes: <a href="https://itunes.apple.com/us/app/wittydoc/id613954583">Link here</a>\r\n\r\n<a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/screen568x5684.jpeg"><img class="alignnone size-medium wp-image-47" alt="screen568x568" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/screen568x5684-169x300.jpeg" width="169" height="300" /></a>', 'WittyDocs - Android, iPhone', '', 'inherit', 'open', 'open', '', '44-revision-v1', '', '', '2013-12-16 09:52:38', '2013-12-16 09:52:38', '', 44, 'http://localhost.mobileantking.com/2013/12/16/44-revision-v1/', 0, 'revision', '', 0),
(49, 1, '2013-12-16 09:54:21', '2013-12-16 09:54:21', 'Features:\r\n1. Create time list\r\n2. Save list with csv format to send email\r\n3. Get weather through current location\r\nTechnologies:\r\n1. Android SDK\r\n2. Integrate library to get weather of yahoo\r\n3. Get current location\r\n\r\n<a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/timetag.png"><img class="alignnone size-medium wp-image-87" alt="timetag" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/timetag-154x300.png" width="154" height="300" /></a>   <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/timetag2.png"><img class="alignnone size-medium wp-image-88" alt="timetag2" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/timetag2-153x300.png" width="153" height="300" /></a>   <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/timetag3.png"><img class="alignnone size-medium wp-image-89" alt="timetag3" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/timetag3-153x300.png" width="153" height="300" /></a>   <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/timetag4.png"><img class="alignnone size-medium wp-image-90" alt="timetag4" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/timetag4-152x300.png" width="152" height="300" /></a>\r\n\r\n&nbsp;', 'TimeTag - Android', '', 'publish', 'open', 'open', '', 'timetag-android', '', '', '2013-12-23 09:15:38', '2013-12-23 09:15:38', '', 0, 'http://localhost.mobileantking.com/?p=49', 6, 'post', '', 0),
(50, 1, '2013-12-16 09:54:21', '2013-12-16 09:54:21', 'Features:\r\n1. Create time list\r\n2. Save list with csv format to send email\r\n3. Get weather through current location\r\nTechnologies:\r\n1. Android SDK\r\n2. Integrate library to get weather of yahoo\r\n3. Get current location\r\n\r\n&nbsp;', 'TimeTag - Android', '', 'inherit', 'open', 'open', '', '49-revision-v1', '', '', '2013-12-16 09:54:21', '2013-12-16 09:54:21', '', 49, 'http://localhost.mobileantking.com/2013/12/16/49-revision-v1/', 0, 'revision', '', 0),
(51, 1, '2013-12-16 09:57:25', '2013-12-16 09:57:25', 'Features:\r\n1. E-Commerce flow\r\n2. Integrate Credit card payment\r\nTechnologies:\r\n1. Prestashop\r\n2. PHP\r\n\r\nGo to site:  <a href="http://www.iloveminime.com/">www.iloveminime.com</a>\r\n\r\n&nbsp;', 'iLoveMiniMe', '', 'trash', 'open', 'open', '', 'iloveminime', '', '', '2013-12-23 04:28:31', '2013-12-23 04:28:31', '', 0, 'http://localhost.mobileantking.com/?p=51', 0, 'post', '', 0),
(52, 1, '2013-12-16 09:57:25', '2013-12-16 09:57:25', 'Features:\r\n1. E-Commerce flow\r\n2. Integrate Credit card payment\r\nTechnologies:\r\n1. Prestashop\r\n2. PHP\r\n\r\nGo to site:  <a href="http://www.iloveminime.com/">www.iloveminime.com</a>\r\n\r\n&nbsp;', 'iLoveMiniMe', '', 'inherit', 'open', 'open', '', '51-revision-v1', '', '', '2013-12-16 09:57:25', '2013-12-16 09:57:25', '', 51, 'http://localhost.mobileantking.com/2013/12/16/51-revision-v1/', 0, 'revision', '', 0),
(53, 1, '2013-12-16 09:59:21', '2013-12-16 09:59:21', 'Features:\r\n1. E-Commerce flow\r\nTechnologies:\r\n1. Magento\r\n2. PHP\r\n\r\nGo to site: <a href="http://rosesonly.com.sg/">rosesonly.com.sg</a>\r\n\r\n', 'RosesOnly', '', 'trash', 'open', 'open', '', 'rosesonly', '', '', '2013-12-23 04:28:31', '2013-12-23 04:28:31', '', 0, 'http://localhost.mobileantking.com/?p=53', 0, 'post', '', 0),
(54, 1, '2013-12-16 09:59:21', '2013-12-16 09:59:21', 'Features:\r\n1. E-Commerce flow\r\nTechnologies:\r\n1. Magento\r\n2. PHP\r\n\r\nGo to site: <a href="http://rosesonly.com.sg/">rosesonly.com.sg</a>\r\n\r\n', 'RosesOnly', '', 'inherit', 'open', 'open', '', '53-revision-v1', '', '', '2013-12-16 09:59:21', '2013-12-16 09:59:21', '', 53, 'http://localhost.mobileantking.com/2013/12/16/53-revision-v1/', 0, 'revision', '', 0),
(55, 1, '2013-12-16 10:01:27', '2013-12-16 10:01:27', 'Features:\r\n1. Blog\r\nTechnologies:\r\n1. Wordpress\r\n2. PHP\r\n\r\nGo to site: <a href="http://tivanity.antking.net/">tivanity.antking.net</a>', 'TiVanity', '', 'trash', 'open', 'open', '', 'tivanity', '', '', '2013-12-23 04:28:30', '2013-12-23 04:28:30', '', 0, 'http://localhost.mobileantking.com/?p=55', 0, 'post', '', 0),
(56, 1, '2013-12-16 10:01:27', '2013-12-16 10:01:27', 'Features:\r\n1. Blog\r\nTechnologies:\r\n1. Wordpress\r\n2. PHP\r\n\r\nGo to site: <a href="http://tivanity.antking.net/">tivanity.antking.net</a>', 'TiVanity', '', 'inherit', 'open', 'open', '', '55-revision-v1', '', '', '2013-12-16 10:01:27', '2013-12-16 10:01:27', '', 55, 'http://localhost.mobileantking.com/2013/12/16/55-revision-v1/', 0, 'revision', '', 0),
(57, 1, '2013-12-16 10:03:06', '2013-12-16 10:03:06', 'Features:\r\n1. Book bus online\r\nTechnologies:\r\n1. Zend\r\n2. PHP\r\n\r\nGo to site:  <a href="http://bookabus.sg/">bookabus.sg</a>', 'Bookabus', '', 'trash', 'open', 'open', '', 'bookabus', '', '', '2013-12-23 04:28:30', '2013-12-23 04:28:30', '', 0, 'http://localhost.mobileantking.com/?p=57', 0, 'post', '', 0),
(58, 1, '2013-12-16 10:03:06', '2013-12-16 10:03:06', 'Features:\r\n1. Book bus online\r\nTechnologies:\r\n1. Zend\r\n2. PHP\r\n\r\nGo to site:  <a href="http://bookabus.sg/">bookabus.sg</a>', 'Bookabus', '', 'inherit', 'open', 'open', '', '57-revision-v1', '', '', '2013-12-16 10:03:06', '2013-12-16 10:03:06', '', 57, 'http://localhost.mobileantking.com/2013/12/16/57-revision-v1/', 0, 'revision', '', 0),
(59, 1, '2013-12-16 10:05:07', '2013-12-16 10:05:07', 'Features:\r\n1. Simple Scrolling website\r\nTechnologies:\r\n1. HTML\r\n2. Jquery\r\n\r\nGo to site:  <a href="http://kienlua.vn/">kienlua.vn</a>', 'KienLua', '', 'trash', 'open', 'open', '', 'kienlua', '', '', '2013-12-23 04:28:30', '2013-12-23 04:28:30', '', 0, 'http://localhost.mobileantking.com/?p=59', 0, 'post', '', 0),
(60, 1, '2013-12-16 10:05:07', '2013-12-16 10:05:07', 'Features:\r\n1. Simple Scrolling website\r\nTechnologies:\r\n1. HTML\r\n2. Jquery\r\n\r\nGo to site:  <a href="http://kienlua.vn/">kienlua.vn</a>', 'KienLua', '', 'inherit', 'open', 'open', '', '59-revision-v1', '', '', '2013-12-16 10:05:07', '2013-12-16 10:05:07', '', 59, 'http://localhost.mobileantking.com/2013/12/16/59-revision-v1/', 0, 'revision', '', 0),
(61, 1, '2013-12-16 10:06:50', '2013-12-16 10:06:50', 'Features:\r\n1. Products show\r\nTechnologies:\r\n1. Prestashop\r\n2. PHP\r\n\r\nGo to site:  <a href="http://yourkitchenhome.com/vn/1-home">yourkitchenhome.com</a>', 'YourKitchenHome', '', 'trash', 'open', 'open', '', 'yourkitchenhome', '', '', '2013-12-23 04:28:29', '2013-12-23 04:28:29', '', 0, 'http://localhost.mobileantking.com/?p=61', 0, 'post', '', 0),
(62, 1, '2013-12-16 10:06:50', '2013-12-16 10:06:50', 'Features:\r\n1. Products show\r\nTechnologies:\r\n1. Prestashop\r\n2. PHP\r\n\r\nGo to site:  <a href="http://yourkitchenhome.com/vn/1-home">yourkitchenhome.com</a>', 'YourKitchenHome', '', 'inherit', 'open', 'open', '', '61-revision-v1', '', '', '2013-12-16 10:06:50', '2013-12-16 10:06:50', '', 61, 'http://localhost.mobileantking.com/2013/12/16/61-revision-v1/', 0, 'revision', '', 0),
(67, 1, '2013-12-23 04:49:48', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2013-12-23 04:49:48', '0000-00-00 00:00:00', '', 0, 'http://localhost.mobileantking.com/?p=67', 0, 'post', '', 0),
(65, 1, '2013-12-16 11:26:26', '2013-12-16 11:26:26', '', 'logo', '', 'inherit', 'open', 'open', '', 'logo-2', '', '', '2013-12-16 11:26:26', '2013-12-16 11:26:26', '', 0, 'http://localhost.mobileantking.com/wp-content/uploads/2013/12/logo.png', 0, 'attachment', 'image/png', 0),
(66, 1, '2013-12-16 12:16:48', '2013-12-16 12:16:48', '', 'logo', '', 'inherit', 'open', 'open', '', 'logo-3', '', '', '2013-12-16 12:16:48', '2013-12-16 12:16:48', '', 0, 'http://localhost.mobileantking.com/wp-content/uploads/2013/12/logo1.png', 0, 'attachment', 'image/png', 0),
(68, 1, '2013-12-23 08:38:32', '2013-12-23 08:38:32', '', 'ODS', '', 'inherit', 'open', 'open', '', 'ods', '', '', '2013-12-23 08:38:32', '2013-12-23 08:38:32', '', 32, 'http://localhost.mobileantking.com/wp-content/uploads/2013/12/ODS.png', 0, 'attachment', 'image/png', 0),
(69, 1, '2013-12-23 08:38:36', '2013-12-23 08:38:36', '', 'ODS2', '', 'inherit', 'open', 'open', '', 'ods2', '', '', '2013-12-23 08:38:36', '2013-12-23 08:38:36', '', 32, 'http://localhost.mobileantking.com/wp-content/uploads/2013/12/ODS2.png', 0, 'attachment', 'image/png', 0),
(70, 1, '2013-12-23 08:38:40', '2013-12-23 08:38:40', '', 'ODS3', '', 'inherit', 'open', 'open', '', 'ods3', '', '', '2013-12-23 08:38:40', '2013-12-23 08:38:40', '', 32, 'http://localhost.mobileantking.com/wp-content/uploads/2013/12/ODS3.png', 0, 'attachment', 'image/png', 0),
(71, 1, '2013-12-23 08:39:03', '2013-12-23 08:39:03', '<a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/ODS.png"><img class="alignnone size-medium wp-image-68" alt="ODS" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/ODS-180x300.png" width="180" height="300" /></a>  <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/ODS3.png"><img class="alignnone size-medium wp-image-70" alt="ODS3" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/ODS3-200x300.png" width="200" height="300" /></a>Features:\n1. Tracking device on map\n2. Push notification\n3. View messages about devices\n4. Add new device\n5. Register new account\n6. Setting for device\n7. Share through FB, Twitter, Google+,...\nTechnologies:\n1. Android, iPhone and WP8 SDK\n2. Cloud Made SDK\n3. Push notification for Android, iOS\n4. Scan QR code\n5. Parse Web service to get data\n6. Animation for left or right swiping\n\nGo to site: <a href="http://ods.covexis.com/">ods.covexis.com</a>', 'ODS-Android, iPhone, WP8', '', 'inherit', 'open', 'open', '', '32-autosave-v1', '', '', '2013-12-23 08:39:03', '2013-12-23 08:39:03', '', 32, 'http://localhost.mobileantking.com/2013/12/23/32-autosave-v1/', 0, 'revision', '', 0),
(72, 1, '2013-12-23 08:39:52', '2013-12-23 08:39:52', 'Features:\r\n1. Tracking device on map\r\n2. Push notification\r\n3. View messages about devices\r\n4. Add new device\r\n5. Register new account\r\n6. Setting for device\r\n7. Share through FB, Twitter, Google+,...\r\nTechnologies:\r\n1. Android, iPhone and WP8 SDK\r\n2. Cloud Made SDK\r\n3. Push notification for Android, iOS\r\n4. Scan QR code\r\n5. Parse Web service to get data\r\n6. Animation for left or right swiping\r\n\r\nGo to site: <a href="http://ods.covexis.com/">ods.covexis.com</a>\r\n\r\n<a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/ODS2.png"><img alt="ODS2" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/ODS2-200x300.png" width="200" height="300" /></a>    <img alt="ODS" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/ODS-180x300.png" width="180" height="300" />    <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/ODS3.png"><img alt="ODS3" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/ODS3-200x300.png" width="200" height="300" /></a>', 'ODS-Android, iPhone, WP8', '', 'inherit', 'open', 'open', '', '32-revision-v1', '', '', '2013-12-23 08:39:52', '2013-12-23 08:39:52', '', 32, 'http://localhost.mobileantking.com/2013/12/23/32-revision-v1/', 0, 'revision', '', 0),
(73, 1, '2013-12-23 08:43:55', '2013-12-23 08:43:55', '', 'alarm', '', 'inherit', 'open', 'open', '', 'alarm', '', '', '2013-12-23 08:43:55', '2013-12-23 08:43:55', '', 30, 'http://localhost.mobileantking.com/wp-content/uploads/2013/12/alarm.png', 0, 'attachment', 'image/png', 0),
(74, 1, '2013-12-23 08:43:59', '2013-12-23 08:43:59', '', 'notification', '', 'inherit', 'open', 'open', '', 'notification', '', '', '2013-12-23 08:43:59', '2013-12-23 08:43:59', '', 30, 'http://localhost.mobileantking.com/wp-content/uploads/2013/12/notification.png', 0, 'attachment', 'image/png', 0),
(75, 1, '2013-12-23 08:44:03', '2013-12-23 08:44:03', '', 'search_location', '', 'inherit', 'open', 'open', '', 'search_location', '', '', '2013-12-23 08:44:03', '2013-12-23 08:44:03', '', 30, 'http://localhost.mobileantking.com/wp-content/uploads/2013/12/search_location.png', 0, 'attachment', 'image/png', 0),
(76, 1, '2013-12-23 08:44:08', '2013-12-23 08:44:08', '', 'set_position', '', 'inherit', 'open', 'open', '', 'set_position', '', '', '2013-12-23 08:44:08', '2013-12-23 08:44:08', '', 30, 'http://localhost.mobileantking.com/wp-content/uploads/2013/12/set_position.png', 0, 'attachment', 'image/png', 0),
(77, 1, '2013-12-23 08:44:12', '2013-12-23 08:44:12', '', 'splash_screen', '', 'inherit', 'open', 'open', '', 'splash_screen', '', '', '2013-12-23 08:44:12', '2013-12-23 08:44:12', '', 30, 'http://localhost.mobileantking.com/wp-content/uploads/2013/12/splash_screen.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(78, 1, '2013-12-23 08:47:02', '2013-12-23 08:47:02', '<p style="text-align: left;">Features:\n1. Choose position on map to alarm\n2. Auto check when user''s phone within radius of Choosed position to alarm\n3. Share through Facebook, Twitter, Google+\nTechnologies:\n1. Android, iPhone &amp; WP8 SDK\n2. Broadcast Receiver to alarm\n3. Cloud Made SDK</p>\nGo to site : <a href="http://puddy.covexis.com">puddy.covexis.com</a>\n\n&nbsp;\n\n<a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/splash_screen.png"><img alt="splash_screen" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/splash_screen-168x300.png" width="168" height="300" /></a>      <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/notification.png"><img class="alignnone size-medium wp-image-74" alt="notification" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/notification-168x300.png" width="168" height="300" /></a>     <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/search_location.png"><img class="alignnone size-medium wp-image-75" alt="search_location" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/search_location-168x300.png" width="168" height="300" /></a>      <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/set_position.png"><img class="alignnone size-medium wp-image-76" alt="set_position" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/set_position-168x300.png" width="168" height="300" /></a>\n\n&nbsp;', 'Puddy - iPhone, Android, WP8', '', 'inherit', 'open', 'open', '', '30-autosave-v1', '', '', '2013-12-23 08:47:02', '2013-12-23 08:47:02', '', 30, 'http://localhost.mobileantking.com/2013/12/23/30-autosave-v1/', 0, 'revision', '', 0),
(80, 1, '2013-12-23 08:53:16', '2013-12-23 08:53:16', '', 'add_server_ip', '', 'inherit', 'open', 'open', '', 'add_server_ip', '', '', '2013-12-23 08:53:16', '2013-12-23 08:53:16', '', 28, 'http://localhost.mobileantking.com/wp-content/uploads/2013/12/add_server_ip.png', 0, 'attachment', 'image/png', 0),
(79, 1, '2013-12-23 08:45:47', '2013-12-23 08:45:47', '<p style="text-align: left;">Features:\r\n1. Choose position on map to alarm\r\n2. Auto check when user''s phone within radius of Choosed position to alarm\r\n3. Share through Facebook, Twitter, Google+\r\nTechnologies:\r\n1. Android, iPhone &amp; WP8 SDK\r\n2. Broadcast Receiver to alarm\r\n3. Cloud Made SDK</p>\r\nGo to site : <a href="http://puddy.covexis.com">puddy.covexis.com</a>\r\n\r\n&nbsp;\r\n\r\n<a href="http://puddy.covexis.com"><a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/alarm.png"><a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/splash_screen.png"><img alt="splash_screen" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/splash_screen-168x300.png" width="168" height="300" /></a>     </a> <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/notification.png"><img class="alignnone size-medium wp-image-74" alt="notification" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/notification-168x300.png" width="168" height="300" /></a>     <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/search_location.png"><img class="alignnone size-medium wp-image-75" alt="search_location" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/search_location-168x300.png" width="168" height="300" /></a>      <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/set_position.png"><img class="alignnone size-medium wp-image-76" alt="set_position" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/set_position-168x300.png" width="168" height="300" /></a> </a>\r\n\r\n&nbsp;', 'Puddy - iPhone, Android, WP8', '', 'inherit', 'open', 'open', '', '30-revision-v1', '', '', '2013-12-23 08:45:47', '2013-12-23 08:45:47', '', 30, 'http://localhost.mobileantking.com/2013/12/23/30-revision-v1/', 0, 'revision', '', 0),
(81, 1, '2013-12-23 08:53:21', '2013-12-23 08:53:21', '', 'choose_canvas_color', '', 'inherit', 'open', 'open', '', 'choose_canvas_color', '', '', '2013-12-23 08:53:21', '2013-12-23 08:53:21', '', 28, 'http://localhost.mobileantking.com/wp-content/uploads/2013/12/choose_canvas_color.png', 0, 'attachment', 'image/png', 0),
(82, 1, '2013-12-23 08:53:27', '2013-12-23 08:53:27', '', 'choose_pen_size', '', 'inherit', 'open', 'open', '', 'choose_pen_size', '', '', '2013-12-23 08:53:27', '2013-12-23 08:53:27', '', 28, 'http://localhost.mobileantking.com/wp-content/uploads/2013/12/choose_pen_size.png', 0, 'attachment', 'image/png', 0),
(83, 1, '2013-12-23 08:57:09', '2013-12-23 08:57:09', '', 'splash_screen', '', 'inherit', 'open', 'open', '', 'splash_screen-2', '', '', '2013-12-23 08:57:09', '2013-12-23 08:57:09', '', 28, 'http://localhost.mobileantking.com/wp-content/uploads/2013/12/splash_screen1.png', 0, 'attachment', 'image/png', 0),
(84, 1, '2013-12-23 09:18:01', '2013-12-23 09:18:01', 'Features:\n1. Draw something on canvas\n2. Save image file by proximity sensor\n3. Send image file to server through TCP/IP socket\nTechnologies:\n1. Android &amp; iPhone SDK\n2. Use canvas to draw\n3. Create TCP/IP socket to transfer file\n4. Use proximity sensor to detect and save drawings\n\n&nbsp;\n\n<a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/splash_screen2.png"><img class="alignnone size-medium wp-image-92" alt="splash_screen" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/splash_screen2-169x300.png" width="169" height="300" /></a>   <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/add_server_ip.png"><img class="alignnone size-medium wp-image-80" alt="add_server_ip" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/add_server_ip-168x300.png" width="168" height="300" /></a>    <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/choose_canvas_color.png"><img class="alignnone size-medium wp-image-81" alt="choose_canvas_color" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/choose_canvas_color-168x300.png" width="168" height="300" /></a>    <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/choose_pen_size.png"><img class="alignnone size-medium wp-image-82" alt="choose_pen_size" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/choose_pen_size-168x300.png" width="168" height="300" /></a>', 'DrawingApps - Android', '', 'inherit', 'open', 'open', '', '28-autosave-v1', '', '', '2013-12-23 09:18:01', '2013-12-23 09:18:01', '', 28, 'http://localhost.mobileantking.com/2013/12/23/28-autosave-v1/', 0, 'revision', '', 0),
(85, 1, '2013-12-23 08:58:51', '2013-12-23 08:58:51', 'Features:\r\n1. Draw something on canvas\r\n2. Save image file by proximity sensor\r\n3. Send image file to server through TCP/IP socket\r\nTechnologies:\r\n1. Android &amp; iPhone SDK\r\n2. Use canvas to draw\r\n3. Create TCP/IP socket to transfer file\r\n4. Use proximity sensor to detect and save drawings\r\n\r\n&nbsp;\r\n\r\n<a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/splash_screen1.png"><img alt="splash_screen" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/splash_screen1-168x300.png" width="168" height="300" /></a>   <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/add_server_ip.png"><img class="alignnone size-medium wp-image-80" alt="add_server_ip" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/add_server_ip-168x300.png" width="168" height="300" /></a>    <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/choose_canvas_color.png"><img class="alignnone size-medium wp-image-81" alt="choose_canvas_color" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/choose_canvas_color-168x300.png" width="168" height="300" /></a>    <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/choose_pen_size.png"><img class="alignnone size-medium wp-image-82" alt="choose_pen_size" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/choose_pen_size-168x300.png" width="168" height="300" /></a>', 'DrawingApps - Android', '', 'inherit', 'open', 'open', '', '28-revision-v1', '', '', '2013-12-23 08:58:51', '2013-12-23 08:58:51', '', 28, 'http://localhost.mobileantking.com/2013/12/23/28-revision-v1/', 0, 'revision', '', 0),
(86, 1, '2013-12-23 09:15:30', '2013-12-23 09:15:30', 'Features:\n1. Create time list\n2. Save list with csv format to send email\n3. Get weather through current location\nTechnologies:\n1. Android SDK\n2. Integrate library to get weather of yahoo\n3. Get current location\n\n<a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/timetag.png"><img class="alignnone size-medium wp-image-87" alt="timetag" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/timetag-154x300.png" width="154" height="300" /></a>   <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/timetag2.png"><img class="alignnone size-medium wp-image-88" alt="timetag2" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/timetag2-153x300.png" width="153" height="300" /></a>   <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/timetag3.png"><img class="alignnone size-medium wp-image-89" alt="timetag3" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/timetag3-153x300.png" width="153" height="300" /></a>   <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/timetag4.png"><img class="alignnone size-medium wp-image-90" alt="timetag4" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/timetag4-152x300.png" width="152" height="300" /></a>\n\n&nbsp;', 'TimeTag - Android', '', 'inherit', 'open', 'open', '', '49-autosave-v1', '', '', '2013-12-23 09:15:30', '2013-12-23 09:15:30', '', 49, 'http://localhost.mobileantking.com/2013/12/23/49-autosave-v1/', 0, 'revision', '', 0),
(87, 1, '2013-12-23 09:14:18', '2013-12-23 09:14:18', '', 'timetag', '', 'inherit', 'open', 'open', '', 'timetag', '', '', '2013-12-23 09:14:18', '2013-12-23 09:14:18', '', 49, 'http://localhost.mobileantking.com/wp-content/uploads/2013/12/timetag.png', 0, 'attachment', 'image/png', 0),
(88, 1, '2013-12-23 09:14:22', '2013-12-23 09:14:22', '', 'timetag2', '', 'inherit', 'open', 'open', '', 'timetag2', '', '', '2013-12-23 09:14:22', '2013-12-23 09:14:22', '', 49, 'http://localhost.mobileantking.com/wp-content/uploads/2013/12/timetag2.png', 0, 'attachment', 'image/png', 0),
(89, 1, '2013-12-23 09:14:25', '2013-12-23 09:14:25', '', 'timetag3', '', 'inherit', 'open', 'open', '', 'timetag3', '', '', '2013-12-23 09:14:25', '2013-12-23 09:14:25', '', 49, 'http://localhost.mobileantking.com/wp-content/uploads/2013/12/timetag3.png', 0, 'attachment', 'image/png', 0),
(90, 1, '2013-12-23 09:14:28', '2013-12-23 09:14:28', '', 'timetag4', '', 'inherit', 'open', 'open', '', 'timetag4', '', '', '2013-12-23 09:14:28', '2013-12-23 09:14:28', '', 49, 'http://localhost.mobileantking.com/wp-content/uploads/2013/12/timetag4.png', 0, 'attachment', 'image/png', 0),
(91, 1, '2013-12-23 09:15:38', '2013-12-23 09:15:38', 'Features:\r\n1. Create time list\r\n2. Save list with csv format to send email\r\n3. Get weather through current location\r\nTechnologies:\r\n1. Android SDK\r\n2. Integrate library to get weather of yahoo\r\n3. Get current location\r\n\r\n<a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/timetag.png"><img class="alignnone size-medium wp-image-87" alt="timetag" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/timetag-154x300.png" width="154" height="300" /></a>   <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/timetag2.png"><img class="alignnone size-medium wp-image-88" alt="timetag2" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/timetag2-153x300.png" width="153" height="300" /></a>   <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/timetag3.png"><img class="alignnone size-medium wp-image-89" alt="timetag3" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/timetag3-153x300.png" width="153" height="300" /></a>   <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/timetag4.png"><img class="alignnone size-medium wp-image-90" alt="timetag4" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/timetag4-152x300.png" width="152" height="300" /></a>\r\n\r\n&nbsp;', 'TimeTag - Android', '', 'inherit', 'open', 'open', '', '49-revision-v1', '', '', '2013-12-23 09:15:38', '2013-12-23 09:15:38', '', 49, 'http://localhost.mobileantking.com/2013/12/23/49-revision-v1/', 0, 'revision', '', 0),
(92, 1, '2013-12-23 09:17:40', '2013-12-23 09:17:40', '', 'splash_screen', '', 'inherit', 'open', 'open', '', 'splash_screen-3', '', '', '2013-12-23 09:17:40', '2013-12-23 09:17:40', '', 28, 'http://localhost.mobileantking.com/wp-content/uploads/2013/12/splash_screen2.png', 0, 'attachment', 'image/png', 0),
(93, 1, '2013-12-23 09:18:19', '2013-12-23 09:18:19', 'Features:\r\n1. Draw something on canvas\r\n2. Save image file by proximity sensor\r\n3. Send image file to server through TCP/IP socket\r\nTechnologies:\r\n1. Android &amp; iPhone SDK\r\n2. Use canvas to draw\r\n3. Create TCP/IP socket to transfer file\r\n4. Use proximity sensor to detect and save drawings\r\n\r\n&nbsp;\r\n\r\n<a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/splash_screen2.png"><img class="alignnone size-medium wp-image-92" alt="splash_screen" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/splash_screen2-169x300.png" width="169" height="300" /></a>   <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/add_server_ip.png"><img class="alignnone size-medium wp-image-80" alt="add_server_ip" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/add_server_ip-168x300.png" width="168" height="300" /></a>    <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/choose_canvas_color.png"><img class="alignnone size-medium wp-image-81" alt="choose_canvas_color" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/choose_canvas_color-168x300.png" width="168" height="300" /></a>    <a href="http://localhost.mobileantking.com/wp-content/uploads/2013/12/choose_pen_size.png"><img class="alignnone size-medium wp-image-82" alt="choose_pen_size" src="http://localhost.mobileantking.com/wp-content/uploads/2013/12/choose_pen_size-168x300.png" width="168" height="300" /></a>', 'DrawingApps - Android', '', 'inherit', 'open', 'open', '', '28-revision-v1', '', '', '2013-12-23 09:18:19', '2013-12-23 09:18:19', '', 28, 'http://localhost.mobileantking.com/2013/12/23/28-revision-v1/', 0, 'revision', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_terms`
--

CREATE TABLE IF NOT EXISTS `wp_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL DEFAULT '',
  `slug` varchar(200) NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'Main Menu', 'main-menu', 0),
(3, 'Native App', 'native-app', 0),
(4, 'E-Commerce web', 'e-commerce-web', 0),
(5, 'Blog', 'blog', 0),
(6, 'Portal', 'portal', 0),
(7, 'Company site', 'company-site', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_relationships`
--

CREATE TABLE IF NOT EXISTS `wp_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(11, 1, 0),
(15, 3, 0),
(12, 3, 0),
(22, 3, 0),
(28, 3, 0),
(30, 3, 0),
(32, 3, 0),
(34, 3, 0),
(39, 3, 0),
(44, 3, 0),
(49, 3, 0),
(51, 4, 0),
(53, 4, 0),
(55, 5, 0),
(57, 6, 0),
(59, 7, 0),
(61, 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_taxonomy`
--

CREATE TABLE IF NOT EXISTS `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) NOT NULL DEFAULT '',
  `description` longtext NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 0),
(2, 2, 'nav_menu', '', 0, 0),
(3, 3, 'category', '', 0, 10),
(4, 4, 'category', '', 0, 0),
(5, 5, 'category', '', 0, 0),
(6, 6, 'category', '', 0, 0),
(7, 7, 'category', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_usermeta`
--

CREATE TABLE IF NOT EXISTS `wp_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'first_name', ''),
(2, 1, 'last_name', ''),
(3, 1, 'nickname', 'admin_name'),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'comment_shortcuts', 'false'),
(7, 1, 'admin_color', 'fresh'),
(8, 1, 'use_ssl', '0'),
(9, 1, 'show_admin_bar_front', 'true'),
(10, 1, 'wp_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(11, 1, 'wp_user_level', '10'),
(12, 1, 'dismissed_wp_pointers', 'wp330_toolbar,wp330_saving_widgets,wp340_choose_image_from_library,wp340_customize_current_theme_link,wp350_media,wp360_revisions,wp360_locks'),
(13, 1, 'show_welcome_panel', '1'),
(14, 1, 'wp_dashboard_quick_press_last_post_id', '67'),
(15, 1, 'wp_user-settings', 'libraryContent=browse&editor=tinymce'),
(16, 1, 'wp_user-settings-time', '1387188183'),
(17, 1, 'managenav-menuscolumnshidden', 'a:4:{i:0;s:11:"link-target";i:1;s:11:"css-classes";i:2;s:3:"xfn";i:3;s:11:"description";}'),
(18, 1, 'metaboxhidden_nav-menus', 'a:2:{i:0;s:8:"add-post";i:1;s:12:"add-post_tag";}'),
(19, 1, 'nav_menu_recently_edited', '2');

-- --------------------------------------------------------

--
-- Table structure for table `wp_users`
--

CREATE TABLE IF NOT EXISTS `wp_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) NOT NULL DEFAULT '',
  `user_pass` varchar(64) NOT NULL DEFAULT '',
  `user_nicename` varchar(50) NOT NULL DEFAULT '',
  `user_email` varchar(100) NOT NULL DEFAULT '',
  `user_url` varchar(100) NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(60) NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$B2clDN2m3NF5iecCrhpqzcKnASxfz8.', 'admin', 'hoangdinh@antking.com.vn', '', '2013-11-20 04:08:47', '', 0, 'admin');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
